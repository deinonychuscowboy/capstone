#!/usr/bin/env python3
print("Loading libraries...")
import os,sys,importlib
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
#os.environ['CUDA_VISIBLE_DEVICES']='-1'
import tensorflow as tf
import warnings
warnings.simplefilter("default")

dataset_name=os.environ["DATASET"] if "DATASET" in os.environ else "np"
epochs=os.environ["EPOCHS"] if "EPOCHS" in os.environ else 10
strategy_name=os.environ["STRATEGY"] if "STRATEGY" in os.environ else "conv"
ds=importlib.import_module("datasets.sent."+dataset_name)
from datasets.sent import dummy
from lib.gan import GANBuilder
from lib.selection import Interpolator
from lib.graph import Graph

tf.get_logger().setLevel('WARN')
tf.autograph.set_verbosity(1)

builder=GANBuilder(metric_size=4,latent_dim=128,global_batch_size=64,epoch_count=epochs,used_graph_layers=2)

dataset,labels,data_shape,codec = ds.corpus()

cond_gan=builder.build(sample_size=data_shape[1],categorical_size=len(labels[0]),channel_size=data_shape[2])

# disable auto-sharding since we will be submitting data one piece at a time
tf.data.experimental.DistributeOptions.auto_shard_policy=tf.data.experimental.AutoShardPolicy.OFF

interp=Interpolator(cond_gan,builder,25)
fake_data,fake_labels=interp.random() #.interpolate_class(0,0)
conv_labels=cond_gan.convert_labels(builder,fake_labels)

print()
for i in range(0,len(fake_data)):
	sample=fake_data[i]
	label=conv_labels[i]
	g=Graph(sample,codec)
	print("This data does not exist:",g.to_str())
	sample_and_labels=tf.concat([
		tf.reshape(sample,(1,builder.sample_size,builder.sample_size,builder.num_channels)),
		tf.reshape(label,(1,builder.sample_size,builder.sample_size,builder.num_classes))
	],-1)
	confidence=cond_gan.discriminator.predict(sample_and_labels,verbose=0)
	print("Does discriminator think this is real? ",confidence)
	print()
	with open(os.path.expanduser("/content/drive/MyDrive/capstone/fake"+str(i)+"_syn.dot"),"w") as f:
		f.write(g.to_dot_syntactic())
	with open(os.path.expanduser("/content/drive/MyDrive/capstone/fake"+str(i)+"_sem.dot"),"w") as f:
		f.write(g.to_dot_semantic())

del dataset
del labels
del data_shape
del codec
tf.keras.backend.clear_session()
dataset,labels,data_shape,codec=dummy.corpus(sample_size=builder.sample_size)
for i in range(0,len(dataset)):
	sample=dataset[i]
	label=labels[i]
	g=Graph(sample,codec)
	print("This is test data:",g.to_str())
	sample_and_labels=tf.concat([
		tf.reshape(sample,(1,builder.sample_size,builder.sample_size,builder.num_channels)),
		tf.reshape(label,(1,builder.sample_size,builder.sample_size,builder.num_classes))
	],-1)
	confidence=cond_gan.discriminator.predict(sample_and_labels,verbose=0)
	print("Does discriminator think this is real? ",confidence)
	print()
	with open(os.path.expanduser("/content/drive/MyDrive/capstone/test"+str(i)+"_syn.dot"),"w") as f:
		f.write(g.to_dot_syntactic())
	with open(os.path.expanduser("/content/drive/MyDrive/capstone/test"+str(i)+"_sem.dot"),"w") as f:
		f.write(g.to_dot_semantic())
