Adv. Computational Linguistcs Term Project / Capstone Project
=============================================================

This is Arthur Domino's capstone (thesis) project for his M.S. in Computational Linguistics, completed May 2023 at the State University of New York at Buffalo.

This project is an implementation of a Generative Adversarial Network (GAN) which, instead of processing images like most GANs, processes a representation of linguistic data. This research was undertaken to explore the potential applications of GAN models to the field of linguistics, and encourage cross-discipline knowledge sharing within the various disciplines which make use of machine learning algorithms.

For (much) more information about the project, see the accompanying paper which was submitted as the other part of the capstone requirements.

Requirements
------------

A linux machine with access to a CUDA-capable GPU and python installed. Google Colab was used for development.

Setup
-----

```
!pip3 install -r requirements.txt
!python3 bin/precache.py # optional
!python3 bin/report.py # optional
```

Environment Variables
---------------------

```
DATASET=np
# which dataset to use, options are coca, coca_fc, npbigrams, np, and dummy

MAXSIZE=64
# permit the number of tokens to be up to this max

MINSIZE=1
# require the number of tokens to be more than this min

EPOCHS=10
# number of epochs to run

STRATEGY=conv
# network type, options are conv, dense, lstm, attn

ACTIVATE=2
# whether/how to activate, options are 0 (no activation), 1 (activate both generator and discriminator), 2 (activate only discriminator), 3 (activate only generator)

ECDP_WEIGHT=0.05
# weight of the penalization function for Excess Channel Depth Perfusion

NSGT_WEIGHT=0.05
# weight of the penalization for Non-Symmetric Graph Template

IGN_WEIGHT=0.05
# weight of the penalization for Identical Graph Nodes

SC_WEIGHT=0.05
# weight of the penalization for Size Calculation

GEN_LEARNING=0.0003
# learning rate of the generator

DISC_LEARNING=0.0003
# learning rate of the discriminator
```

Data Preparation
----------------

```
# Prepare the first 1000 records of the coca corpus and save to google drive.
!python3 bin/prepare_data.py 0 1000
# parameter 1 is the record to start from, parameter 2 is the batch size to use, so if you want 1000 more records, run
!python3 bin/prepare_data.py 1000 1000
# a batch size of larger than 1000 is not recommended, run multiple batches to get large corpus sizes
!for x in {0..19000..1000}; bin/python3 prepare_data.py $x 1000; done
```

Running the GAN
---------------

```
!python3 bin/train.py
!python3 bin/generate.py
```
