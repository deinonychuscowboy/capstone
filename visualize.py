#!/usr/bin/env python3

print("Loading libraries...")
import os,sys,importlib
import warnings
import matplotlib
from matplotlib import pyplot
from lib.graph import Graph
warnings.simplefilter("default")

dataset_name=os.environ["DATASET"] if "DATASET" in os.environ else "np"
epochs=int(os.environ["EPOCHS"]) if "EPOCHS" in os.environ else 10
strategy_name=os.environ["STRATEGY"] if "STRATEGY" in os.environ else "conv"
ds=importlib.import_module("datasets.sent."+dataset_name)

dataset,labels,data_shape,codec = ds.corpus()

matplotlib.rc("font",size=6)

for r in dataset:
	f=pyplot.figure(figsize=(3840/80,2160/80),dpi=80)
	g=Graph(r,codec)
	f.suptitle(g.to_str())
	p=f.add_subplot(3,2,(1,4),projection="3d")
	p.title.set_text("Vector Representation")
	for x in range(0,r.shape[0]):
		for y in range(0,r.shape[1]):
			for z in range(0,r.shape[2]):
				value=r[x,r.shape[1]-1-y,z]
				if value==0.:
					pass
				else:
					p.text(x/r.shape[0],y/r.shape[1],z/r.shape[2],f"{value:.1f}")
	p=f.add_subplot(325)
	p.title.set_text("Syntax Layer")
	for x in range(1,r.shape[0]):
		for y in range(0,r.shape[1]-1):
			value=r[x,r.shape[1]-1-y,0]
			if value==0.:
				pass
			else:
				p.text(x/r.shape[0],y/r.shape[1],f"{value:.1f}")
	p=f.add_subplot(326)
	p.title.set_text("Semantics Layer")
	for x in range(1,r.shape[0]):
		for y in range(0,r.shape[1]-1):
			value=r[x,r.shape[1]-1-y,1]
			if value==0.:
				pass
			else:
				p.text(x/r.shape[0],y/r.shape[1],f"{value:.1f}")
	pyplot.show()
