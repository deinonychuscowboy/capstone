#!/usr/bin/env python3
print("Loading libraries...")
import os,sys,importlib
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
#os.environ['CUDA_VISIBLE_DEVICES']='-1'
import tensorflow as tf
import warnings, pickle, graphviz
from matplotlib import pyplot
from inputimeout import inputimeout
warnings.simplefilter("default")

dataset_name=os.environ["DATASET"] if "DATASET" in os.environ else "np"
epochs=os.environ["EPOCHS"] if "EPOCHS" in os.environ else 10
strategy_name=os.environ["STRATEGY"] if "STRATEGY" in os.environ else "conv"
ds=importlib.import_module("datasets.sent."+dataset_name)
import datasets
from datasets.sent import dummy
from lib.gan import GANBuilder
from lib.selection import Interpolator
from lib.graph import Graph

tf.get_logger().setLevel('WARN')
tf.autograph.set_verbosity(1)

builder=GANBuilder(metric_size=4,latent_dim=128,global_batch_size=64,epoch_count=epochs,used_graph_layers=2)

perfusion_loss_weight=float(os.environ["ECDP_WEIGHT"]) if "ECDP_WEIGHT" in os.environ else 0.05
symmetric_loss_weight=float(os.environ["NSGT_WEIGHT"]) if "NSGT_WEIGHT" in os.environ else 0.05
identical_loss_weight=float(os.environ["IGN_WEIGHT"]) if "IGN_WEIGHT" in os.environ else 0.05
size_loss_weight=float(os.environ["SC_WEIGHT"]) if "SC_WEIGHT" in os.environ else 0.05
activation="2" if "ACTIVATE" not in os.environ else os.environ["ACTIVATE"]
maxsize=64 if "MAXSIZE" not in os.environ else int(os.environ["MAXSIZE"])
minsize=1 if "MINSIZE" not in os.environ else int(os.environ["MINSIZE"])

checkpoint_location=os.path.expanduser(datasets.datacache+dataset_name+"_"+str(epochs)+"_"+strategy_name+"_"+activation+"_"+str(perfusion_loss_weight)+"_"+str(symmetric_loss_weight)+"_"+str(identical_loss_weight)+"_"+str(size_loss_weight)+"/")
from lib.tee import Tee
sys.stdout = Tee(sys.stdout, open(checkpoint_location+"generation.log", "w"))
sys.stderr = Tee(sys.stderr, open(checkpoint_location+"generation.err", "w"))

codec = ds.get_codec()
with open(checkpoint_location+"data_shape","rb") as f:
	data_shape, categories=pickle.load(f)

cond_gan=builder.build(sample_size=data_shape[1],categorical_size=categories,channel_size=data_shape[2])
print("Loading checkpoint...")

cond_gan.load_weights(checkpoint_location+"checkpoint")

# disable auto-sharding since we will be submitting data one piece at a time
tf.data.experimental.DistributeOptions.auto_shard_policy=tf.data.experimental.AutoShardPolicy.OFF

length=2 if "DATASET" in os.environ and os.environ["DATASET"]=="npbigrams" else 5 if "DATASET" in os.environ and os.environ["DATASET"]=="np" else None

def generate(dataset,labels,index,dtype,codec):
	global length
	global checkpoint_location
	sample=dataset[index]
	label=labels[index]
	if length is None:
		g=Graph(sample,codec,override_size=sample.shape[0]-1 if dtype=="fake" else None)
		print("This data does not exist:",g.to_str())
	if dtype=="fake" and length is None:
		try:
			length=int(inputimeout("Change the length?",timeout=30))
			if length is None:
				length=sample.shape[0]-1
		except:
			length=sample.shape[0]-1
	if length is not None:
		g=Graph(sample,codec,override_size=int(length) if dtype=="fake" else None)
		print("This data does not exist:",g.to_str())
	sample_and_labels=tf.concat([
		tf.reshape(sample,(1,builder.sample_size,builder.sample_size,builder.num_channels)),
		tf.reshape(label,(1,builder.sample_size,builder.sample_size,builder.num_classes))
	],-1)
	confidence=cond_gan.discriminator.predict(sample_and_labels,verbose=0)
	print("Does discriminator think this is fake? ",confidence)
	print()
	with open(os.path.expanduser(checkpoint_location+dtype+str(index)+"_syn.dot"),"w") as f:
		f.write(g.to_dot_syntactic())
	graphviz.render("dot",filepath=os.path.expanduser(checkpoint_location+dtype+str(index)+"_syn.dot"),outfile=os.path.expanduser(checkpoint_location+dtype+str(index)+"_syn.pdf"))
	os.unlink(os.path.expanduser(checkpoint_location+dtype+str(index)+"_syn.dot"))
	with open(os.path.expanduser(checkpoint_location+dtype+str(index)+"_sem.dot"),"w") as f:
		f.write(g.to_dot_semantic())
	graphviz.render("dot",filepath=os.path.expanduser(checkpoint_location+dtype+str(index)+"_sem.dot"),outfile=os.path.expanduser(checkpoint_location+dtype+str(index)+"_sem.pdf"))
	os.unlink(os.path.expanduser(checkpoint_location+dtype+str(index)+"_sem.dot"))

	f=pyplot.figure(figsize=(3840/80,2160/80),dpi=80)
	f.suptitle(g.to_str())
	p=f.add_subplot(3,2,(1,4),projection="3d")
	p.title.set_text("Vector Representation")
	r=sample
	for x in range(0,r.shape[0]):
		for y in range(0,r.shape[1]):
			for z in range(0,r.shape[2]):
				value=r[x,r.shape[1]-1-y,z]
				if value==0.:
					pass
				else:
					p.text(x/r.shape[0],y/r.shape[1],z/r.shape[2],f"{value:.1f}")
	p=f.add_subplot(325)
	p.title.set_text("Syntax Layer")
	for x in range(1,r.shape[0]):
		for y in range(0,r.shape[1]-1):
			value=r[x,r.shape[1]-1-y,0]
			if value==0.:
				pass
			else:
				p.text(x/r.shape[0],y/r.shape[1],f"{value:.1f}")
	p=f.add_subplot(326)
	p.title.set_text("Semantics Layer")
	for x in range(1,r.shape[0]):
		for y in range(0,r.shape[1]-1):
			value=r[x,r.shape[1]-1-y,1]
			if value==0.:
				pass
			else:
				p.text(x/r.shape[0],y/r.shape[1],f"{value:.1f}")
	pyplot.savefig(os.path.expanduser(checkpoint_location+dtype+str(index)+"_vector.png"))
	pyplot.close(f)

interp=Interpolator(cond_gan,builder,25)
fake_data,fake_labels=interp.random() #.interpolate_class(0,0)
conv_labels=cond_gan.convert_labels(builder,fake_labels)

print()
for i in range(0,len(fake_data)):
	generate(fake_data,conv_labels,i,"fake",codec)

del codec
tf.keras.backend.clear_session()

dataset,labels,data_shape,codec=dummy.corpus(maxsize=maxsize,minsize=minsize)
conv_labels=cond_gan.convert_labels(builder,labels)
for i in range(0,len(dataset)):
	generate(dataset,conv_labels,i,"test",codec)