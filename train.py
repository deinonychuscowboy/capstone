#!/usr/bin/env python3
print("Loading libraries...")
import os,sys,importlib
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
#os.environ['CUDA_VISIBLE_DEVICES']='-1'
import tensorflow as tf
import warnings, pickle
import datasets
warnings.simplefilter("default")

dataset_name=os.environ["DATASET"] if "DATASET" in os.environ else "np"
epochs=int(os.environ["EPOCHS"]) if "EPOCHS" in os.environ else 10
strategy_name=os.environ["STRATEGY"] if "STRATEGY" in os.environ else "conv"
ds=importlib.import_module("datasets.sent."+dataset_name)

from lib.tee import Tee
perfusion_loss_weight=float(os.environ["ECDP_WEIGHT"]) if "ECDP_WEIGHT" in os.environ else 0.05
symmetric_loss_weight=float(os.environ["NSGT_WEIGHT"]) if "NSGT_WEIGHT" in os.environ else 0.05
identical_loss_weight=float(os.environ["IGN_WEIGHT"]) if "IGN_WEIGHT" in os.environ else 0.05
size_loss_weight=float(os.environ["SC_WEIGHT"]) if "SC_WEIGHT" in os.environ else 0.05
activation="2" if "ACTIVATE" not in os.environ else os.environ["ACTIVATE"]
checkpoint_location=os.path.expanduser(datasets.datacache+dataset_name+"_"+str(epochs)+"_"+strategy_name+"_"+activation+"_"+str(perfusion_loss_weight)+"_"+str(symmetric_loss_weight)+"_"+str(identical_loss_weight)+"_"+str(size_loss_weight)+"/")
os.makedirs(checkpoint_location, exist_ok=True)
sys.stdout = Tee(sys.stdout, open(checkpoint_location+"training.log", "w"))
sys.stderr = Tee(sys.stderr, open(checkpoint_location+"training.err", "w"))

from lib.gan import GANBuilder

tf.get_logger().setLevel('WARN')
tf.autograph.set_verbosity(1)

builder=GANBuilder(metric_size=4,latent_dim=128,global_batch_size=64,epoch_count=epochs,used_graph_layers=2)

dataset,labels,data_shape,codec = ds.corpus()

cond_gan=builder.build(sample_size=data_shape[1],categorical_size=len(labels[0]),channel_size=data_shape[2])
print("Beginning training...")

print(dataset.shape)
print(labels.shape)
cond_gan.fit(dataset, labels, epochs=builder.epoch_count, batch_size=builder.global_batch_size)

cond_gan.save_weights(checkpoint_location+"checkpoint")
with open(checkpoint_location+"data_shape","wb") as f:
	pickle.dump((data_shape,len(labels[0])),f)
print("Training complete!")
