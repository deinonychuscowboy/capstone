#!/usr/bin/env python3
import sys, warnings, os, importlib
warnings.simplefilter("default")

dataset_name=os.environ["DATASET"] if "DATASET" in os.environ else "np"
maxsize=64 if "MAXSIZE" not in os.environ else int(os.environ["MAXSIZE"])
minsize=1 if "MINSIZE" not in os.environ else int(os.environ["MINSIZE"])
ds=importlib.import_module("datasets.sent."+dataset_name)

start=0
length=100
if len(sys.argv)>=2:
	start=int(sys.argv[1])
if len(sys.argv)>=3:
	length=int(sys.argv[2])

ds.prepare(start,length,maxsize,minsize)
