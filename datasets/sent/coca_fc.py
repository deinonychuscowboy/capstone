from . import *

def corpus():
	return process_corpus(None,"coca_fc")

def prepare(start=0,len=1000,maxsize=64,minsize=1):
	corpus=download_coca()
	codec,bp,stog=get_models("coca_fc",spec_codec="fasttext:wiki.en",spec_syn="ud:UD_English-EWT/en_ewt-ud-train.conllu")
	prepare_corpus(corpus[start:start+len],"coca_fc",bp,stog,codec, maxsize=maxsize,minsize=minsize)

def get_codec():
	return get_models("coca_fc",spec_codec="fasttext:wiki.en",spec_syn="ud:UD_English-EWT/en_ewt-ud-train.conllu")[0]
