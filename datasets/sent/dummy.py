from . import *

dummy_corpus=[
	# Templatic sentences
	("The quick brown fox jumps over the lazy dog.",0),
	("Jackdaws just love my big sphinx of quartz.",0),
	# Simple sentences
	("Mary had a little lamb.",0),
	("Twinkle, twinkle, little star.",0),
	# complex sentences
	("Yesterday I went to the store on the corner and bought bananas.",0),
	("One day I would like to visit the ocean and swim for hours.",0),
	# Basic errors
	("Banana banana banana banana banana banana banana.",0),
	("Runs hamsteak the free the porcelain sleeting housing.",0),
	# Syntax errors
	("He will the race long tomorrow run.",0),
	("Went Maria the store to.",0),
	# Semantic errors
	("Colorless green ideas sleep furiously.",0),
	("He tore the idea up herself.",0),
	# valid npbigrams
	("compact disc",0),
	("taco bell",0),
	# invalid npbigrams
	("too early",0), # no noun
	("to Mary",0), # prepositional phrase
	# valid nptrigrams
	("too much fun",0),
	("a way forward",0),
	# invalid nptrigrams
	("early late never",0), # no noun
	("on the top",0), # prepositional phrase
	# valid np4grams
	("the lake erie watershed",0),
	("just the facts please",0),
	# invalid np4grams
	("am quickly running away",0), # no noun
	("in the first place",0), # prepositional phrase
	# valid np5grams
	("a freshly made steak sandwich",0),
	("the north campus bus stop",0),
	# invalid np5grams
	("eagerly seeking and impatiently waiting",0), # no noun
	("from sea to shining sea",0), # prepositional phrase
]

def corpus(maxsize=64,minsize=1):
	return process_corpus([x[0] for x in dummy_corpus],"dummy",save=False, maxsize=maxsize,minsize=minsize)

def get_codec():
	return get_models("dummy")[0]
