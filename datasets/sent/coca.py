from . import *

def corpus():
	return process_corpus(None,"coca")

def prepare(start=0,len=1000,maxsize=64,minsize=1):
	corpus=download_coca()
	codec,bp,stog=get_models("coca")
	prepare_corpus(corpus[start:start+len],"coca",bp,stog,codec, maxsize=maxsize,minsize=minsize)

def get_codec():
	return get_models("coca")[0]
