from . import *

def corpus():
	return process_corpus(None,"npbigrams")

def prepare(start=0,len=1000,maxsize=64,minsize=1):
	corpus=filter_nps(download_ngrams())
	codec,syn,sem=get_models("npbigrams",spec_syn="annotated")
	prepare_corpus(corpus[start:start+len],"npbigrams",syn,sem,codec,annotated=True, maxsize=maxsize,minsize=minsize)

def filter_nps(corpus):
	out=[]
	for record in corpus:
		include=True
		if record[-1].endswith(".NOUN") and len(record)==2:
			for word in record:
				if word.endswith(".ADP"):
					include=False
			if include:
				out.append(record)
	return out

def get_codec():
	return get_models("npbigrams",spec_syn="annotated")[0]
