from typing import *

import traceback
import gzip, tarfile
import gensim.downloader
import gensim.models
import penman
import warnings
import os, shutil
import copy

import psutil
import tensorflow as tf

from lib.graph import Graph

from .. import *

def process_corpus(corpus,name,save=True,maxsize=64,minsize=1):
	codec,bp,stog=get_models(name,save=save)

	vectors,labels,size=get_corpus(corpus,name,bp,stog,codec,save=save,maxsize=maxsize,minsize=minsize)

	return vectors,labels,size,codec

def get_corpus(corpus,name,bp,stog,codec,save=True,maxsize=64,minsize=1):
	os.makedirs(os.path.expanduser(datacache),exist_ok=True)
	size=tuple()
	if not os.path.exists(os.path.expanduser(""+datacache+"data_"+name+".tar")):
		assert corpus is not None, "No corpus provided! This probably means that you haven't prepared any cached data yet."
		vectors,labels,size=prepare_corpus(corpus,name,bp,stog,codec,save=save,maxsize=maxsize,minsize=minsize)
	else:
		print("Loading data...")
		split=0
		vectors=None
		labels=None
		done=False
		starting_ram=psutil.virtual_memory().total-psutil.virtual_memory().available
		with tarfile.open(os.path.expanduser(""+datacache+"data_"+name+".tar"),"r:") as f:
			if os.path.exists(os.path.expanduser(workingdir)):
				shutil.rmtree(os.path.expanduser(workingdir))
			os.makedirs(os.path.expanduser(workingdir))
			f.extractall(workingdir)
		while os.path.exists(os.path.expanduser(""+workingdir+"data_"+name+"_"+str(split)+".gz")) and not done:
			with gzip.open(os.path.expanduser(""+workingdir+"data_"+name+"_"+str(split)+".gz")) as f:
				vectors_,labels_,size=pickle.load(f)
				if vectors is not None:
					offset=vectors.shape[0]
					vectors.resize((vectors.shape[0]+vectors_.shape[0],max(vectors.shape[1],vectors_.shape[1]),max(vectors.shape[2],vectors_.shape[2]),max(vectors.shape[3],vectors_.shape[3])),refcheck=False)
					for i in range(0,vectors_.shape[0]):
						for j in range(0,vectors_.shape[1]):
							for k in range(0,vectors_.shape[2]):
								for l in range(0,vectors_.shape[3]):
									vectors[i+offset,j,k,l]=vectors_[i,j,k,l]
					current_ram=psutil.virtual_memory().total-psutil.virtual_memory().available-starting_ram
					print(len(vectors),current_ram/1024/1024/1024)
					if psutil.virtual_memory().total-(current_ram*2+starting_ram)<1024*1024*1024*memlimit and split>0:
						done=True
					del vectors_
				else:
					vectors=vectors_.copy()
					del vectors_
				if labels is not None:
					offset=labels.shape[0]
					labels.resize((labels.shape[0]+labels_.shape[0],max(labels.shape[1],labels_.shape[1])),refcheck=False)
					for i in range(0,labels_.shape[0]):
						for j in range(0,labels_.shape[1]):
							labels[i+offset,j]=labels_[i,j]
					del labels_
				else:
					labels=labels_.copy()
					del labels_
				split+=1
	return vectors,labels,size

def prepare_corpus(corpus,name,bp,stog,codec,save=True,annotated=False,maxsize=64,minsize=1):
	os.makedirs(os.path.expanduser(datacache),exist_ok=True)
	os.makedirs(os.path.expanduser(workingdir),exist_ok=True)
	vectors=[]
	labels=[]
	vector_depth=0
	vector_diameter=2
	max_vector_diameter=maxsize
	min_content_length=minsize
	pp={}

	corpus_state:List[List[Union[None,bool]]]=[[None,None,None,None] for x in corpus]

	print("Preprocessing...")
	pos=[]
	if annotated:
		newcorpus=[]
		for record in corpus:
			newword=[]
			newpos=[]
			for word in record:
				w=".".join(word.split(".")[:-1])
				p=word.split(".")[-1]
				newword.append(w)
				newpos.append(p)
			newcorpus.append(" ".join(newword))
			pos.append(newpos)
		corpus=newcorpus
	for i in range(0,len(corpus)):
		if i%100==0:
			print(str(i)+"/"+str(len(corpus)))
		x=corpus[i]
		p=gensim.utils.simple_preprocess(x,deacc=True,min_len=1,max_len=30)
		pp[i]=p
		while min(len(p)+1,max_vector_diameter)>vector_diameter:
			vector_diameter*=2
		corpus_state[i][0]=len(p)<vector_diameter and len(p)>=min_content_length
		if annotated:
			corpus_state[i][0]=corpus_state[i][0] and len(p)==len(pos[i])
	print("Syntax computation...")
	spar={}
	for i in range(0,len(corpus)):
		if i%100==0:
			print(str(i)+"/"+str(len(corpus)))
		state=corpus_state[i]
		if state[0] is not None and state[0]:
			if len(pp[i])==0:
				corpus_state[i][1]=False
				spar[i]=None
			else:
				if annotated:
					spar[i]=pos[i]
					corpus_state[i][1]=True
				else:
					b=list(bp.parse_sents([pp[i]]))
					if len(b)==0:
						spar[i]=None
						corpus_state[i][1]=False
					else:
						spar[i]=b[0]
						corpus_state[i][1]=True
	print("Semantic computation...")
	epar={}
	for i in range(0,len(corpus)):
		if i%10==0:
			print(str(i)+"/"+str(len(corpus)))
		state=corpus_state[i]
		if state[1] is not None and state[1]:
			x=corpus[i]
			g=stog.parse_sents([x],add_metadata=True)
			if len(g)==0:
				epar[i]=None
				corpus_state[i][2]=False
			else:
				pg=penman.decode(g[0])
				pg.metadata['snt']=x
				epar[i]=pg
				corpus_state[i][2]=True
	print("Graph conversion...")
	for i in range(0,len(corpus)):
		if i%100==0:
			print(str(i)+"/"+str(len(corpus)))
		state=corpus_state[i]
		if state[2] is not None and state[2]:
			pg=epar[i]
			x=pp[i]
			if not annotated:
				b=tree_to_graph(spar[i])
			else:
				b=(spar[i],[],spar[i])
			if len(x)>0:
				sem={}
				syn={}
				vec=[]
				lab={}
				snodes={}
				enodes={}
				usages=[]
				for j in range(0,len(x)):
					y=x[j]
					se=(None,None,y)
					enode=None
					nodes=[y for x in pg.variables() for y in pg.triples if x==y[0] and y[1]==":instance"]
					nodes.reverse()
					for trip in nodes:
						if trip[2] is not None and trip[2].startswith(y):
							# this is a very naive way to get the node but seems like the only way since amrlib does not provide order information
							se=trip
							enode=trip[0]
							break
					if not annotated:
						sy,snode=get_node(b,y,copy.deepcopy(usages)) # this will probably work if the tree is always in sentence order, but still naive
					else:
						sy=b[0][j]
						snode=[[]]
					usages.append(y)
					vrep=None
					try:
						vrep=codec[y]
						corpus_state[i][3]=True
					except KeyError:
						vrep=codec["##"]
						corpus_state[i][3]=False
					if numpy.isnan(vrep.sum()):
						pass
					vec.append(vrep)
					sem[j]=se
					syn[j]=sy
					lab[j]=y
					snodes[j]=snode
					enodes[j]=enode
				vec={x:vec[x] for x in range(0,len(vec))}

				vector_depth=vec[0].shape[0]
				vector=None
				try:
					vector=Graph(vec,syn,sem,lab,snodes,enodes,b,pg,codec).to_vector(vector_diameter,vector_depth)
					if numpy.isnan(vector.sum()):
						raise Exception("nan in vector graph")
					# print(str(lab.values()))
					# print(str(b))
					# print(str(snodes))
					# print(str(syn))
					# print(str(pr.sub(" ",str(penman.encode(pg,compact=False,indent=0)))))
					# print(str(enodes))
					# print(str(sem))
					# print()
				except Exception as e:
					corpus_state[i][3]=False
					warnings.warn(traceback.format_exc())
				if corpus_state[i][3]:
					vectors.append(vector)
					# op_complexity=0
					# arg_complexity=0
					# syn_complexity=int(list(b[0].keys())[-1]/20)
					# for x in pg.triples:
					# 	if x[1] is not None:
					# 		if x[1].startswith(":op"):
					# 			c=int(int(ii.sub("",x[1].split("-")[0][3:]))/4)
					# 			if c>op_complexity:
					# 				op_complexity=c
					# 		if x[1].startswith(":ARG"):
					# 			c=int(int(ii.sub("",x[1].split("-")[0][4:]))/2)
					# 			if c>arg_complexity:
					# 				arg_complexity=c
					labels.append(0) #syn_complexity+op_complexity*8+arg_complexity*8*4)
					print(labels[-1]," ".join(list(lab.values())))
	vectors=numpy.array(vectors,dtype='float32')
	labels=tf.keras.utils.to_categorical(labels,dtype='float32')
	size=(vector_diameter,vector_diameter,vector_depth)
	print("Finished data preparation!")
	i=0
	done=False
	created_first=False
	split=0
	if save:
		while not done:
			if not os.path.exists(os.path.expanduser(""+workingdir+"data_"+name+"_"+str(i)+".gz")):
				with gzip.open(os.path.expanduser(""+workingdir+"data_"+name+"_"+str(i)+".gz"),"wb") as f:
					pickle.dump((vectors[split:split+100],labels[split:split+100],size),f)
					split+=100
					if split>=len(vectors):
						done=True
					if i==0:
						created_first=True
			i+=1
		if created_first and os.path.exists(os.path.expanduser(""+datacache+"data_"+name+".tar")):
			os.unlink(os.path.expanduser(""+datacache+"data_"+name+".tar"))
		with tarfile.open(os.path.expanduser(""+datacache+"data_"+name+".tar"),"a:") as f:
			i=0
			while os.path.exists(os.path.expanduser(""+workingdir+"data_"+name+"_"+str(split)+".gz")) and not done:
				f.addfile(f.getarinfo(os.path.expanduser(""+workingdir+"data_"+name+"_"+str(split)+".gz")))
				i+=1
	return vectors,labels,size

def get_node(tree,label,usages=None)->Tuple:
	if usages is None:
		usages=[]
	for x,y in tree[0].items():
		if x in tree[2] and tree[2][x]==label:
			if label in usages:
				usages.remove(label)
			else:
				for edge in tree[1]:
					if edge[1]==x:
						return y,x
	return None,None

def tree_to_graph(tree,parent=None,nodeids=None,nodes=None,edges=None,leaves=None):
	if nodes is None:
		nodes={}
	if edges is None:
		edges=[]
	if leaves is None:
		leaves={}
	if nodeids is None:
		nodeids=[]
	if parent is None:
		nodes[0]=tree._label
		parent=0
		nodeids.append(0)
	cur_children={}
	for node in tree:
		if len(cur_children)>0 and len(tree)>2:
			# convert from non-binary-branching tree to binary-branching tree
			newp=nodeids[-1]+1
			nodeids.append(newp)
			nodes[newp]=nodes[parent]
			edges.append((parent,newp))
			parent=newp
		id=nodeids[-1]+1
		nodeids.append(id)
		cur_children[id]=node
		if (parent,id) not in edges and parent!=id:
			edges.append((parent,id))
		if type(node) is str:
			leaves[id]=node
			nodes[id]=tree._label
		else:
			nodes[id]=node._label
	for id,node in cur_children.items():
		if type(node) is not str:
			tree_to_graph(node,id,nodeids,nodes,edges,leaves)
	return nodes,edges,leaves
