import gensim
import gensim.downloader
import gensim.models
import numpy
import benepar
import os.path
import urllib.request
import tarfile
import zipfile
import pickle
import re
import random
import pyconll
import gzip
import unicodedata
import subprocess
import nltk
import amrlib

cleanup=re.compile(r"  +")
datacache="/content/drive/MyDrive/capstone/"
workingdir="/content/cache/"
#datacache="~/.cache/capstone/"
memlimit=float(os.environ["MEMLIMIT"]) if "MEMLIMIT" in os.environ else 1.0
pr=re.compile(r"\w\w+")
ii=re.compile(r"\D+")

def download_coca():
	if not os.path.exists(os.path.expanduser("~/.cache/coca/coca.txt")):
		print("Downloading coca corpus...")
		os.makedirs(os.path.expanduser("~/.cache/coca/"),exist_ok=True)
		urllib.request.urlretrieve("https://www.corpusdata.org/coca/samples/coca-samples-text.zip",os.path.expanduser("~/.cache/coca/coca.zip"))
		with open(os.path.expanduser("~/.cache/coca/coca.zip"),"rb") as f:
			zipfile.ZipFile(f).extractall(os.path.expanduser("~/.cache/coca/"))
		os.unlink(os.path.expanduser("~/.cache/coca/coca.zip"))
		# remove coca elements that are hard for benepar and AMRlib to handle
		os.unlink(os.path.expanduser("~/.cache/coca/text_fic.txt"))
		os.unlink(os.path.expanduser("~/.cache/coca/text_mag.txt"))
		os.unlink(os.path.expanduser("~/.cache/coca/text_spok.txt"))
		os.unlink(os.path.expanduser("~/.cache/coca/text_tvm.txt"))
		corpus=[]
		random.seed(0)
		with open(os.path.expanduser("~/.cache/coca/coca.txt"),"w") as f:
			for path,dirs,files in os.walk(os.path.expanduser("~/.cache/coca/")):
				for file in files:
					if file!="coca.txt":
						with open(path+file,"r") as g:
							for text in g:
								for sentence in nltk.tokenize.sent_tokenize(text[1:]): # skip document ID
									corpus.append(sentence)
						os.unlink(path+file)
			random.shuffle(corpus)
			for x in corpus:
				if "@" not in x:
					f.write(x.replace("<p>","").replace("<s>","")+"\n")

	text=""
	with open(os.path.expanduser("~/.cache/coca/coca.txt"),"r") as f:
		text=f.readlines()
	return [x for x in text]

def download_udtb():
	if not os.path.exists(os.path.expanduser("~/.cache/udtb/ud-treebanks-v2.11")):
		print("Downloading universal dependencies corpus...")
		os.makedirs(os.path.expanduser("~/.cache/udtb/"),exist_ok=True)
		urllib.request.urlretrieve("https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-4923/ud-treebanks-v2.11.tgz?sequence=1&isAllowed=y",os.path.expanduser("~/.cache/udtb/udtb.tgz"))
		subprocess.run(["tar","-xzf",os.path.expanduser("~/.cache/udtb/udtb.tgz")])
		os.unlink(os.path.expanduser("~/.cache/udtb/udtb.tgz"))

def download_stog(model,version):
	if not os.path.exists(os.path.expanduser("~/.cache/amrlib/"+model)):
		os.makedirs(os.path.expanduser("~/.cache/amrlib/"),exist_ok=True)
		urllib.request.urlretrieve("https://github.com/bjascob/amrlib-models/releases/download/"+model+"-"+version+"/model_"+model+"-"+version+".tar.gz",os.path.expanduser("~/.cache/amrlib/"+model+".tar.gz"))
		tarfile.open(os.path.expanduser("~/.cache/amrlib/"+model+".tar.gz"),"r").extractall(os.path.expanduser("~/.cache/amrlib/"))
		os.unlink(os.path.expanduser("~/.cache/amrlib/"+model+".tar.gz"))
		os.rename(os.path.expanduser("~/.cache/amrlib/model_"+model+"-"+version),os.path.expanduser("~/.cache/amrlib/"+model))
	return amrlib.load_stog_model(model_dir=os.path.expanduser("~/.cache/amrlib/"+model))

def download_ngrams():
	if not os.path.exists(os.path.expanduser("~/.cache/coca_ngrams/ngrams.txt")):
		print("Downloading coca ngrams corpus...")
		os.makedirs(os.path.expanduser("~/.cache/coca_ngrams/"),exist_ok=True)
		urllib.request.urlretrieve("https://www.ngrams.info/coca/samples/samples_word.zip",os.path.expanduser("~/.cache/coca_ngrams/ngrams.zip"))
		with open(os.path.expanduser("~/.cache/coca_ngrams/ngrams.zip"),"rb") as f:
			zipfile.ZipFile(f).extractall(os.path.expanduser("~/.cache/coca_ngrams/"))
		os.unlink(os.path.expanduser("~/.cache/coca_ngrams/ngrams.zip"))
		# ignore files that don't have part of speech
		os.unlink(os.path.expanduser("~/.cache/coca_ngrams/coca_ngrams_x2w.txt"))
		os.unlink(os.path.expanduser("~/.cache/coca_ngrams/coca_ngrams_x3w.txt"))
		os.unlink(os.path.expanduser("~/.cache/coca_ngrams/coca_ngrams_x4w.txt"))
		os.unlink(os.path.expanduser("~/.cache/coca_ngrams/coca_ngrams_x5w.txt"))
		corpus=[]
		# convert part of speech
		pos_map={
			"a":"ADJ",
			"i":"ADP",
			"r":"ADV",
			"c":"CCONJ",
			"n":"NOUN",
			"d":"DET",
			"v":"VERB",
	        "p":"PRON",
		}
		with open(os.path.expanduser("~/.cache/coca_ngrams/ngrams.txt"),"w") as f:
			for path,dirs,files in os.walk(os.path.expanduser("~/.cache/coca_ngrams/")):
				for file in files:
					if file!="ngrams.txt":
						with open(path+file,"r",errors="ignore") as g:
							for record in g:
								unicodedata.normalize("NFKC",record).encode("ascii","replace")
								data=record.split("\t")
								data=data[1:]
								data=list(zip(data[:int(len(data)/2)],data[int(len(data)/2):]))
								ngram=[]
								for word,pos in data:
									if pos is None or pos.strip() not in pos_map:
										break
									ngram.append(word.strip()+"."+pos_map[pos.strip()])
								if len(ngram)==len(data) and len(ngram)>0:
									f.write(" ".join(ngram)+"\n")
						os.unlink(path+file)

	text=""
	with open(os.path.expanduser("~/.cache/coca_ngrams/ngrams.txt"),"r") as f:
		text=f.readlines()
	return [x.strip().split(" ") for x in text]

def get_models(name,save=True,spec_codec="w2v:glove-wiki-gigaword-50",spec_syn="benepar:benepar_en3",spec_sem='amrlib:parse_xfm_bart_base'):
	os.makedirs(os.path.expanduser(datacache),exist_ok=True)
	if not os.path.exists(os.path.expanduser(""+datacache+"models_"+name+".gz")):
		print("Caching lexical model for ["+name+"] corpus...")
		codec=load_lexical_model(spec_codec)
		syn=None
		sem=None
		if spec_syn is not None:
			print("Caching syntactic model for ["+name+"] corpus...")
			syn=load_syntactic_model(spec_syn)
		if spec_sem is not None:
			print("Caching semantic model for ["+name+"] corpus...")
			sem=load_semantic_model(spec_sem)
		if save:
			with gzip.open(os.path.expanduser(""+datacache+"models_"+name+".gz"),"wb") as f:
				pickle.dump((codec,syn,sem),f)
	else:
		print("Loading precached models for ["+name+"] corpus...")
		with gzip.open(os.path.expanduser(""+datacache+"models_"+name+".gz"),"rb") as f:
			codec,syn,sem=pickle.load(f)

	return codec,syn,sem

def load_semantic_model(spec_sem):
	stog=None
	if spec_sem.startswith("amrlib"):
		spec_sem=spec_sem.split(":")[-1]
		stog=download_stog(spec_sem,'v0_1_0')
	return stog

def load_syntactic_model(spec_syn):
	bp=None
	if spec_syn.startswith("benepar"):
		spec_syn=spec_syn.split(":")[-1]
		benepar.download(spec_syn)
		bp=benepar.Parser(spec_syn)
	elif spec_syn.startswith("ud"):
		spec_syn=spec_syn.split(":")[-1]
		download_udtb()
		bp=pyconll.load_from_file(os.path.expanduser("~/.cache/udtb/ud-treebanks-v2.11/"+spec_syn))
	elif spec_syn.startswith("annotated"):
		return None
	return bp

def load_lexical_model(spec_codec):
	codec=None
	if spec_codec.startswith("w2v"):
		spec_codec=spec_codec.split(":")[-1]
		codec=gensim.downloader.load(spec_codec)
		if "</s>" not in codec:
			codec.add_vector("</s>",numpy.array([0 for x in codec["the"]]))
		if "##" not in codec:
			codec.add_vector("##",numpy.array([0.5 for x in codec["the"]]))
		if "@" not in codec:
			codec.add_vector("@",numpy.array([0.5 for x in codec["the"]]))
	elif spec_codec.startswith("fasttext"):
		spec_codec=spec_codec.split(":")[-1]
		path=os.path.expanduser(os.path.dirname(os.path.dirname(datacache))+"/fasttext/"+spec_codec+".bin")
		os.makedirs(os.path.dirname(path),exist_ok=True)
		subprocess.run(["wget","-nc","-O","/tmp/"+spec_codec+".zip","https://dl.fbaipublicfiles.com/fasttext/vectors-wiki/"+spec_codec+".zip"])
		subprocess.run(["unzip","-n",spec_codec+".zip"],cwd="/tmp/")
		subprocess.run(["mv","/tmp/"+spec_codec+".bin",path])
		os.unlink("/tmp/"+spec_codec+".zip")
		os.unlink("/tmp/"+spec_codec+".vec")
		print("Loading lexical model...")
		codec=gensim.models.FastText.load_fasttext_format(path).wv
	return codec
