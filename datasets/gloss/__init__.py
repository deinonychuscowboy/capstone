import os.path

import psutil, gzip

from .. import *

def process_corpus(corpus,name,save=True,components_size=2,sentence_size=2):
	os.makedirs(os.path.expanduser(datacache),exist_ok=True)
	codec,bp,stog=get_models(name,save=save,spec_codec="fasttext:wiki.en",spec_syn=None,spec_sem=None)

	dataset1,labels1,dataset2,labels2,dataset3,labels3,data_shape,codec=get_corpus(corpus,name,codec,save=save,components_size=components_size,sentence_size=sentence_size)

	return dataset1,labels1,dataset2,labels2,dataset3,labels3,data_shape,codec

def get_corpus(corpus,name,codec,save=True,components_size=2,sentence_size=2):
	os.makedirs(os.path.expanduser(datacache),exist_ok=True)
	size=tuple()
	if not os.path.exists(os.path.expanduser(""+datacache+"_"+name+"_0.gz")):
		assert corpus is not None, "No corpus provided!"
		vectors,size=prepare_corpus(corpus,name,codec,save=save,components_size=components_size,sentence_size=sentence_size)
	else:
		print("Loading data...")
		split=0
		vectors=None
		done=False
		starting_ram=psutil.virtual_memory().total-psutil.virtual_memory().available
		while os.path.exists(os.path.expanduser(""+datacache+"_"+name+"_"+str(split)+".gz")) and not done:
			with gzip.open(os.path.expanduser(""+datacache+"_"+name+"_"+str(split)+".gz"),"rb") as f:
				out,size=pickle.load(f)
				if vectors is not None:
					tmp=numpy.concatenate((vectors,out))
					current_ram=psutil.virtual_memory().total-psutil.virtual_memory().available-starting_ram
					print(current_ram/1024/1024/1024)
					if psutil.virtual_memory().total-(current_ram*2+starting_ram)<1024*1024*1024*memlimit and split>0:
						done=True
					del out
					del vectors
					vectors=tmp
					del tmp
				else:
					vectors=out
					del out
				split+=1
	out1=[]
	out2=[]
	out3=[]
	for x in vectors:
		out1.append(x[0])
		out2.append(x[1])
		out3.append(x[2])
	return numpy.array([x[0] for x in out1]),numpy.array([x[1] for x in out1]),numpy.array([x[0] for x in out2]),numpy.array([x[1] for x in out2]),numpy.array([x[0] for x in out3]),numpy.array([x[1] for x in out3]),size,codec

def prepare_corpus(corpus,name,codec,save=True,components_size=2,sentence_size=2):
	out=[]
	max_components_size=8
	max_sentence_size=16
	for x in corpus:
		for k in range(0,len(x)):
			words=x[k].split(" ")
			while len(words)>sentence_size:
				sentence_size*=2
			for i in range(0,len(words)):
				words[i]=words[i].split(".")
				while len(words[i])>components_size:
					components_size*=2
			x[k]=words
	for x in corpus:
		for k in range(0,len(x)):
			words=x[k]
			for i in range(0,len(words)):
				for j in range(0,len(words[i])):
					words[i][j]=codec[words[i][j]]
				while len(words[i])<min(components_size,max_components_size):
					words[i].append(numpy.array([0 for x in words[i][0]],dtype="float32"))
			while len(words)<min(sentence_size,max_sentence_size):
				d=[]
				for i in range(0,min(components_size,max_components_size)):
					d.append(numpy.array([0 for x in words[0][0]],dtype="float32"))
				words.append(d)
			x[k]=numpy.array(words[:min(sentence_size,max_sentence_size)])
	for x in corpus:
		tmp=[]
		for i in range(0,len(x)-1):
			data=x[i]
			label=x[i+1]
			tmp.append((data,label))
		out.append(tmp)

	size=(min(sentence_size,max_sentence_size),min(components_size,max_components_size),len(out[0][0][0][0][0]))

	i=0
	done=False
	split=0
	if save:
		while not done:
			if not os.path.exists(os.path.expanduser(""+datacache+"_"+name+"_"+str(i)+".gz")):
				with gzip.open(os.path.expanduser(""+datacache+"_"+name+"_"+str(i))+".gz","wb") as f:
					pickle.dump((out[split:split+100],size),f)
					split+=100
					if split>=len(out):
						done=True
			i+=1

	return out,size

def decode(sample,codec):
	ns=[]
	for record in sample:
		nr=""
		for word in record:
			nw=""
			for component in word:
				nw+=codec[component]+"."
			nw=nw[:-1]
			nr+=nw+" "
		nr=nr[:-1]
		ns.append(nr)
	return ns
