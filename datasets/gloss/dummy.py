from . import *

dummy_corpus=[
	[
		"the tree fell across the clearing",
		"the tree.M.SG.NOM fall.PAST across the clearing.N.SG.ACC",
		"the tree.M.SG.NOM fall.PAST across the clearing.N.SG.ACC", # no order difference
		"der baum fallte über das abholzen"
	],
	[
		"because i bought a computer, i couldn't afford my groceries",
		"because P1.SG.NOM buy.PAST a computer.M.SG.ACC, P1.SG.NOM can.PAST not afford P1.SG.POS groceries.PL.ACC",
		"because P1.SG.NOM a computer.M.SG.ACC buy.PAST, can.PAST P1.SG.NOM not P1.SG.DAT.REF P1.SG.POS groceries.PL.ACC afford",
		"weil ich einen computer kaufte, könnte ich nicht mir meine lebensmittel leisten"
	]
]

def corpus():
	return process_corpus(dummy_corpus,"dummy_gloss")
