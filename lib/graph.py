
import numpy, warnings, unicodedata, copy

MAPPED_LAYER_DEPTH=2
EDGE_TYPE_NO_RELATION=0
EDGE_TYPE_SYN_MIN=100
EDGE_TYPE_SYN_UNSPECIFIED=100
EDGE_TYPE_SYN_01=101
EDGE_TYPE_SYN_02=102
EDGE_TYPE_SYN_03=103
EDGE_TYPE_SYN_04=104
EDGE_TYPE_SYN_05=105
EDGE_TYPE_SYN_06=106
EDGE_TYPE_SYN_07=107
EDGE_TYPE_SYN_08=108
EDGE_TYPE_SYN_09=109
EDGE_TYPE_SYN_10=110
EDGE_TYPE_SYN_11=111
EDGE_TYPE_SYN_12=112
EDGE_TYPE_SYN_13=113
EDGE_TYPE_SYN_14=114
EDGE_TYPE_SYN_15=115
EDGE_TYPE_SYN_16=116
EDGE_TYPE_SYN_17=117
EDGE_TYPE_SYN_18=118
EDGE_TYPE_SYN_19=119
EDGE_TYPE_SYN_20=120
EDGE_TYPE_SYN_21=121
EDGE_TYPE_SYN_22=122
EDGE_TYPE_SYN_23=123
EDGE_TYPE_SYN_24=124
EDGE_TYPE_SYN_25=125
EDGE_TYPE_SYN_26=126
EDGE_TYPE_SYN_27=127
EDGE_TYPE_SYN_28=128
EDGE_TYPE_SYN_29=129
EDGE_TYPE_SYN_30=130
EDGE_TYPE_SYN_31=131
EDGE_TYPE_SYN_32=132
EDGE_TYPE_SYN_33=133
EDGE_TYPE_SYN_34=134
EDGE_TYPE_SYN_35=135
EDGE_TYPE_SYN_36=136
EDGE_TYPE_SYN_37=137
EDGE_TYPE_SYN_38=138
EDGE_TYPE_SYN_39=139
EDGE_TYPE_SYN_40=140
EDGE_TYPE_SYN_MAX=140
EDGE_TYPE_SEM_MIN=200
EDGE_TYPE_SEM_UNSPECIFIED=200
EDGE_TYPE_SEM_DEGREE=201
EDGE_TYPE_SEM_DOMAIN=202
EDGE_TYPE_SEM_MOD=203
EDGE_TYPE_SEM_LOCATION=204
EDGE_TYPE_SEM_PART=205
EDGE_TYPE_SEM_POSS=206
EDGE_TYPE_SEM_TIME=207
EDGE_TYPE_SEM_TOPIC=208
EDGE_TYPE_SEM_ARG0=209
EDGE_TYPE_SEM_ARG1=210
EDGE_TYPE_SEM_ARG2=211
EDGE_TYPE_SEM_ARG3=212
EDGE_TYPE_SEM_ARG4=213
EDGE_TYPE_SEM_ARG5=214
EDGE_TYPE_SEM_ARG6=215
EDGE_TYPE_SEM_ARG7=216
EDGE_TYPE_SEM_CONSIST=217
EDGE_TYPE_SEM_FREQUENCY=218
EDGE_TYPE_SEM_OP0=219
EDGE_TYPE_SEM_OP1=220
EDGE_TYPE_SEM_OP2=221
EDGE_TYPE_SEM_OP3=222
EDGE_TYPE_SEM_OP4=223
EDGE_TYPE_SEM_OP5=224
EDGE_TYPE_SEM_OP6=225
EDGE_TYPE_SEM_OP7=226
EDGE_TYPE_SEM_OP8=227
EDGE_TYPE_SEM_OP9=228
EDGE_TYPE_SEM_OP10=229
EDGE_TYPE_SEM_OP11=230
EDGE_TYPE_SEM_OP12=231
EDGE_TYPE_SEM_OP13=232
EDGE_TYPE_SEM_OP14=233
EDGE_TYPE_SEM_OP15=234
EDGE_TYPE_SEM_PURPOSE=235
EDGE_TYPE_SEM_QUANT=236
EDGE_TYPE_SEM_MANNER=237
EDGE_TYPE_SEM_MEDIUM=238
EDGE_TYPE_SEM_DURATION=239
EDGE_TYPE_SEM_ORD=240
EDGE_TYPE_SEM_DIRECTION=241
EDGE_TYPE_SEM_CONDITION=242
EDGE_TYPE_SEM_BENEFICIARY=243
EDGE_TYPE_SEM_WITHOUT=244
EDGE_TYPE_SEM_SOURCE=245
EDGE_TYPE_SEM_CONCESSION=246
EDGE_TYPE_SEM_DESTINATION=247
EDGE_TYPE_SEM_IN=248
EDGE_TYPE_SEM_UNIT=249
EDGE_TYPE_SEM_AS=250
EDGE_TYPE_SEM_INSTRUMENT=251
EDGE_TYPE_SEM_EXAMPLE=252
EDGE_TYPE_SEM_WITH=253
EDGE_TYPE_SEM_FOR=254
EDGE_TYPE_SEM_TO=255
EDGE_TYPE_SEM_PATH=256
EDGE_TYPE_SEM_EXTENT=257
EDGE_TYPE_SEM_ACCOMPANIER=258
EDGE_TYPE_SEM_SUBEVENT=259
EDGE_TYPE_SEM_INSTANCE=260
EDGE_TYPE_SEM_SELF=261
EDGE_TYPE_SEM_MAX=261

class Graph:
	def __init__(self,*args,**kwargs):
		self.lexical_node_info=[]
		self.semantic_node_info={}
		self.syntax_node_info={}
		self.semantic_graph=[]
		self.syntax_tree=None
		self.syntax_graph=[]
		self.lexical_nodes={}
		self.semantic_nodes={}
		self.syntax_nodes={}
		self.phrase_binding={}
		self.codec=None
		if len(args)==2:
			# got a tensor/array input and a codec
			self.codec=args[1]
			self.__from_vector(args[0],**kwargs)
		else:
			assert len(args)==9, "Wrong number of values given to graph"
			self.lexical_node_info=args[0]
			self.semantic_node_info=args[2]
			self.syntax_node_info=args[1]
			self.lexical_nodes=args[3]
			self.semantic_nodes=args[5]
			self.syntax_nodes=args[4]
			self.semantic_graph=args[7]
			self.syntax_tree=args[6]
			self.codec=args[8]

	def to_vector(self,vector_diameter,vector_depth):
		self.__flatten_tree()
		vector=[[[len(self.lexical_nodes)/vector_diameter]]]
		for j in range(1,vector_depth): # build the corner depth-column
			vector[0][0].append(0)
		vector[0][0]=numpy.array(vector[0][0],dtype='float32')
		for j in range(0,vector_diameter-1): # build the top index row
			vector[0].append(self.lexical_node_info[j] if j<len(self.lexical_node_info) else self.codec["</s>"])
		for j in range(0,vector_diameter-1): # build the contents of the graph
			grow=[self.lexical_node_info[j] if j<len(self.lexical_node_info) else self.codec["</s>"]] # first column is the left index row element
			for k in range(0,j): # build depth-columns before self row (j->k)
				d=[]
				for l in range(0,MAPPED_LAYER_DEPTH):
					d.append(self.__determine_edge_type(l,j,k))
				for l in range(MAPPED_LAYER_DEPTH, vector_depth):
					d.append(EDGE_TYPE_NO_RELATION)
				grow.append(numpy.array(d,dtype='float32'))
			d=[] # build depth columns for self row (j==k)
			for l in range(0,MAPPED_LAYER_DEPTH):
				d.append(self.__determine_edge_type(l,j,j))
			for l in range(MAPPED_LAYER_DEPTH, vector_depth):
				d.append(EDGE_TYPE_NO_RELATION)
			grow.append(numpy.array(d,dtype='float32'))
			for k in range(j+1,vector_diameter-1): # build depth columns after self row (k->j)
				d=[]
				for l in range(0,MAPPED_LAYER_DEPTH):
					d.append(self.__determine_edge_type(l,j,k))
				for l in range(MAPPED_LAYER_DEPTH, vector_depth):
					d.append(EDGE_TYPE_NO_RELATION)
				grow.append(numpy.array(d,dtype='float32'))
			vector.append(grow)

		return numpy.array(vector,dtype='float32')

	def __from_vector(self,vector,**kwargs):
		self.lexical_node_info=[]
		self.lexical_nodes={}
		last_node=""
		last_depth=[]
		lexical_node_count=round(vector[0][0][0]*vector.shape[0]) if "override_size" not in kwargs or not kwargs["override_size"] else kwargs["override_size"]
		for i in range(0,vector.shape[0]):
			if i!=0 and i<=lexical_node_count:
				x=vector[i]
				y=vector[0][i]
				depth=[]
				for j in range(0,vector.shape[2]):
					x2=x[0][j]
					y2=y[j]
					depth.append((x2+y2)/2.)
				depth=numpy.array(depth)
				node=unicodedata.normalize("NFKC",self.codec.similar_by_vector(depth,topn=1)[0][0]).encode("ascii","replace")
				if node==last_node:
					warnings.warn("Identical nodes:\n"+str(last_node)+" "+str(last_depth.mean())+"\n"+str(node)+" "+str(depth.mean()))
					#break
				self.lexical_node_info.append(depth)
				self.lexical_nodes[i-1]=node
				last_node=node
				last_depth=depth
		syntax_graph=[]
		semantic_graph=[]
		for i in range(0,len(self.lexical_nodes)):
			for j in range(0,len(self.lexical_nodes)):
				if i==j:
					val=self.__decode_syntax_info(vector[i+1][j+1][0])
					if val is not None:
						self.syntax_node_info[i]=val
					self.semantic_node_info[i]=EDGE_TYPE_SEM_SELF #self.__decode_semantic_info(vector[i+1][j+1][0])
				else:
					val=self.__decode_syntax_level(vector[i+1][j+1][0])
					if val is not None:
						syntax_graph.append((i,val,j))
					val=self.__decode_semantic_level(vector[i+1][j+1][1])
					if val is not None:
						semantic_graph.append((i,val,j))
		self.semantic_graph=semantic_graph # TODO this could be given back to penman
		self.syntax_graph=syntax_graph

	def __flatten_tree(self):
		leaf_levels={x:0 for x in self.syntax_tree[2]}
		for leaf in self.syntax_tree[2]:
			cur_node=leaf
			level=0
			for edge in self.syntax_tree[1]:
				if cur_node==edge[1]:
					cur_node=edge[0]
					level+=1
			leaf_levels[leaf]=level
		leaves=list(leaf_levels.keys())
		leaves.sort(reverse=True,key=lambda x: leaf_levels[x])

		explored=[]
		for leaf in leaves:
			explored.append(leaf)
			level,src_head=self.__find_head(leaf,copy.deepcopy(explored))
			if src_head is not None:
				self.phrase_binding[leaf]=(level,src_head)

	def __find_head(self,src_synid,explored):
		cur_synid=src_synid
		src_head=None
		level=0
		while src_head is None:
			parent=None
			for edge in self.syntax_tree[1]:
				if edge[1]==cur_synid:  # parent of src
					level+=1
					parent=edge[0]
					explored.append(parent)
					descendants=self.__get_descendants(parent,self.syntax_tree)
					for descendant in descendants:
						if descendant not in explored:
							if self.__is_lexical(descendant,self.syntax_tree):
								if self.__is_head(descendant,src_synid,self.syntax_tree):
									src_head=descendant
									break
							explored.append(descendant)
					if src_head is None:
						cur_synid=parent
					break
			if parent is None:
				# reached top of graph, have to descend into other child to find head
				for edge in self.syntax_tree[1]:
					if edge[0]==cur_synid and edge[1] not in explored:
						level+=1
						parent=edge[1]
						explored.append(parent)
						descendants=self.__get_descendants(parent,self.syntax_tree)
						for descendant in descendants:
							if descendant not in explored:
								if self.__is_lexical(descendant,self.syntax_tree):
									if self.__is_head(descendant,src_synid,self.syntax_tree):
										src_head=descendant
										break
								explored.append(descendant)
						if src_head is None:
							cur_synid=parent
						break
			if parent is None or src_head is not None:
				break
		return level,src_head

	def __get_descendants(self,parent,graph):
		expand=[parent]
		descendants=[]

		while len(expand)>0:
			for edge in graph[1]:
				if edge[0]==expand[0] and edge[1] not in descendants and edge[1] not in expand:
					descendants.append(edge[1])
					expand.append(edge[1])
			expand.pop(0)

		return descendants

	def __is_lexical(self,node,graph):
		return node in graph[2]

	def __reduce(self,type):
		if type in ("NN","NNS","EX","WP","WP$","NNP","NNPS","FW","RP","SYM","UH","LS","."):
			type="NN"
		elif type in ("DT","WDT","PDT"):
			type="DT"
		elif type in ("VB","VBD","VBZ","VBG","VBP","MD","VBN"):
			type="VB"
		elif type in ("IN","TO","PRP","PRP$",):
			type="PRP"
		elif type in ("JJ","JJR","JJS","CD"):
			type="JJ"
		elif type in ("CC"):
			type="CC"
		elif type in ("RB","WRB","RBR","RBS"):
			type="RB"
		# else:
		# 	print("UNK "+type)
		return type

	def __is_head(self,head,referent,graph):
		head_info=self.__reduce(graph[0][head])
		referent_info=self.__reduce(graph[0][referent])

		# TODO this is too simplistic, we really need to analyze the whole graph

		if head_info=="NN" and referent_info=="JJ":
			return True
		if head_info=="NN" and referent_info=="DT":
			return True
		if head_info=="VB" and referent_info=="NN":
			return True
		if head_info=="VB" and referent_info=="RB":
			return True
		if head_info=="PRP" and referent_info=="NN":
			return True
		if head_info=="NN" and referent_info=="PRP":
			return True
		if head_info=="VB" and referent_info=="PRP":
			return True
		#print("HEAD? "+head_info+graph[2][head]+" "+referent_info+graph[2][referent])

		return False

	def __determine_semantic_edge_type(self,src_node_id,dest_node_id,src_semvar,dest_semvar):
		if src_node_id==dest_node_id:
			return EDGE_TYPE_NO_RELATION
		else:
			if src_semvar is None or dest_semvar is None:
				return EDGE_TYPE_NO_RELATION
			for x in self.semantic_graph.triples:
				if x[0]==src_semvar and x[2]==dest_semvar:
					return (self.__encode_semantic_level(x)-EDGE_TYPE_SEM_MIN)/(EDGE_TYPE_SEM_MAX-EDGE_TYPE_SEM_MIN)
		return EDGE_TYPE_NO_RELATION

	def __encode_semantic_level(self,x):
		if x[1]==":degree":
			return EDGE_TYPE_SEM_DEGREE
		if x[1]==":domain":
			return EDGE_TYPE_SEM_DOMAIN
		if x[1]==":mod":
			return EDGE_TYPE_SEM_MOD
		if x[1]==":part":
			return EDGE_TYPE_SEM_PART
		if x[1]==":poss":
			return EDGE_TYPE_SEM_POSS
		if x[1]==":time":
			return EDGE_TYPE_SEM_TIME
		if x[1]==":topic":
			return EDGE_TYPE_SEM_TOPIC
		if x[1]==":ARG0":
			return EDGE_TYPE_SEM_ARG0
		if x[1]==":ARG1":
			return EDGE_TYPE_SEM_ARG1
		if x[1]==":ARG2":
			return EDGE_TYPE_SEM_ARG2
		if x[1]==":ARG3":
			return EDGE_TYPE_SEM_ARG3
		if x[1]==":ARG4":
			return EDGE_TYPE_SEM_ARG4
		if x[1]==":ARG5":
			return EDGE_TYPE_SEM_ARG5
		if x[1]==":ARG6":
			return EDGE_TYPE_SEM_ARG6
		if x[1]==":ARG7":
			return EDGE_TYPE_SEM_ARG7
		if x[1]==":consist":
			return EDGE_TYPE_SEM_CONSIST
		if x[1]==":frequency":
			return EDGE_TYPE_SEM_FREQUENCY
		if x[1]==":op0":
			return EDGE_TYPE_SEM_OP0
		if x[1]==":op1":
			return EDGE_TYPE_SEM_OP1
		if x[1]==":op2":
			return EDGE_TYPE_SEM_OP2
		if x[1]==":op3":
			return EDGE_TYPE_SEM_OP3
		if x[1]==":op4":
			return EDGE_TYPE_SEM_OP4
		if x[1]==":op5":
			return EDGE_TYPE_SEM_OP5
		if x[1]==":op6":
			return EDGE_TYPE_SEM_OP6
		if x[1]==":op7":
			return EDGE_TYPE_SEM_OP7
		if x[1]==":op8":
			return EDGE_TYPE_SEM_OP8
		if x[1]==":op9":
			return EDGE_TYPE_SEM_OP9
		if x[1]==":op10":
			return EDGE_TYPE_SEM_OP10
		if x[1]==":op11":
			return EDGE_TYPE_SEM_OP11
		if x[1]==":op12":
			return EDGE_TYPE_SEM_OP12
		if x[1]==":op13":
			return EDGE_TYPE_SEM_OP13
		if x[1]==":op14":
			return EDGE_TYPE_SEM_OP14
		if x[1]==":op15":
			return EDGE_TYPE_SEM_OP15
		if x[1]==":location":
			return EDGE_TYPE_SEM_LOCATION
		if x[1]==":purpose":
			return EDGE_TYPE_SEM_PURPOSE
		if x[1]==":quant":
			return EDGE_TYPE_SEM_QUANT
		if x[1]==":manner":
			return EDGE_TYPE_SEM_MANNER
		if x[1]==":medium":
			return EDGE_TYPE_SEM_MEDIUM
		if x[1]==":duration":
			return EDGE_TYPE_SEM_DURATION
		if x[1]==":ord":
			return EDGE_TYPE_SEM_ORD
		if x[1]==":direction":
			return EDGE_TYPE_SEM_DIRECTION
		if x[1]==":condition":
			return EDGE_TYPE_SEM_CONDITION
		if x[1]==":beneficiary":
			return EDGE_TYPE_SEM_BENEFICIARY
		if x[1]==":prep-without":
			return EDGE_TYPE_SEM_WITHOUT
		if x[1]==":source":
			return EDGE_TYPE_SEM_SOURCE
		if x[1]==":destination":
			return EDGE_TYPE_SEM_DESTINATION
		if x[1]==":unit":
			return EDGE_TYPE_SEM_UNIT
		if x[1]==":concession":
			return EDGE_TYPE_SEM_CONCESSION
		if x[1]==":prep-in":
			return EDGE_TYPE_SEM_IN
		if x[1]==":prep-as":
			return EDGE_TYPE_SEM_AS
		if x[1]==":instrument":
			return EDGE_TYPE_SEM_INSTRUMENT
		if x[1]==":example":
			return EDGE_TYPE_SEM_EXAMPLE
		if x[1]==":prep-with":
			return EDGE_TYPE_SEM_WITH
		if x[1]==":prep-for":
			return EDGE_TYPE_SEM_FOR
		if x[1]==":prep-to":
			return EDGE_TYPE_SEM_TO
		if x[1]==":path":
			return EDGE_TYPE_SEM_PATH
		if x[1]==":extent":
			return EDGE_TYPE_SEM_EXTENT
		if x[1]==":accompanier":
			return EDGE_TYPE_SEM_ACCOMPANIER
		if x[1]==":subevent":
			return EDGE_TYPE_SEM_SUBEVENT
		if x[1]==":instance":
			return EDGE_TYPE_SEM_INSTANCE
		warnings.warn("Missing semantic edge type encoding for "+str(x[1])+" ( "+str(x[0])+", "+str(x[2])+" )")
		return EDGE_TYPE_SEM_UNSPECIFIED

	def __decode_semantic_level(self,x):
		x=x*(EDGE_TYPE_SEM_MAX-EDGE_TYPE_SEM_MIN)+EDGE_TYPE_SEM_MIN
		if x<EDGE_TYPE_SEM_MIN-1/2. or x>EDGE_TYPE_SEM_MAX+1/2.:
			#warnings.warn("Missing semantic edge type decoding for "+str(x))
			return None
		if x<=EDGE_TYPE_SEM_UNSPECIFIED+1/2.:
			return None
		if x<=EDGE_TYPE_SEM_DEGREE+1/2.:
			return ":degree"
		if x<=EDGE_TYPE_SEM_DOMAIN+1/2.:
			return ":domain"
		if x<=EDGE_TYPE_SEM_MOD+1/2.:
			return ":mod"
		if x<=EDGE_TYPE_SEM_PART+1/2.:
			return ":part"
		if x<=EDGE_TYPE_SEM_POSS+1/2.:
			return ":poss"
		if x<=EDGE_TYPE_SEM_TIME+1/2.:
			return ":time"
		if x<=EDGE_TYPE_SEM_TOPIC+1/2.:
			return ":topic"
		if x<=EDGE_TYPE_SEM_ARG0+1/2.:
			return ":ARG0"
		if x<=EDGE_TYPE_SEM_ARG1+1/2.:
			return ":ARG1"
		if x<=EDGE_TYPE_SEM_ARG2+1/2.:
			return ":ARG2"
		if x<=EDGE_TYPE_SEM_ARG3+1/2.:
			return ":ARG3"
		if x<=EDGE_TYPE_SEM_ARG4+1/2.:
			return ":ARG4"
		if x<=EDGE_TYPE_SEM_ARG5+1/2.:
			return ":ARG5"
		if x<=EDGE_TYPE_SEM_ARG6+1/2.:
			return ":ARG6"
		if x<=EDGE_TYPE_SEM_ARG7+1/2.:
			return ":ARG7"
		if x<=EDGE_TYPE_SEM_CONSIST+1/2.:
			return ":consist"
		if x<=EDGE_TYPE_SEM_FREQUENCY+1/2.:
			return ":frequency"
		if x<=EDGE_TYPE_SEM_OP0+1/2.:
			return ":op0"
		if x<=EDGE_TYPE_SEM_OP1+1/2.:
			return ":op1"
		if x<=EDGE_TYPE_SEM_OP2+1/2.:
			return ":op2"
		if x<=EDGE_TYPE_SEM_OP3+1/2.:
			return ":op3"
		if x<=EDGE_TYPE_SEM_OP4+1/2.:
			return ":op4"
		if x<=EDGE_TYPE_SEM_OP5+1/2.:
			return ":op5"
		if x<=EDGE_TYPE_SEM_OP6+1/2.:
			return ":op6"
		if x<=EDGE_TYPE_SEM_OP7+1/2.:
			return ":op7"
		if x<=EDGE_TYPE_SEM_OP8+1/2.:
			return ":op8"
		if x<=EDGE_TYPE_SEM_OP9+1/2.:
			return ":op9"
		if x<=EDGE_TYPE_SEM_OP10+1/2.:
			return ":op10"
		if x<=EDGE_TYPE_SEM_OP11+1/2.:
			return ":op11"
		if x<=EDGE_TYPE_SEM_OP12+1/2.:
			return ":op12"
		if x<=EDGE_TYPE_SEM_OP13+1/2.:
			return ":op13"
		if x<=EDGE_TYPE_SEM_OP14+1/2.:
			return ":op14"
		if x<=EDGE_TYPE_SEM_OP15+1/2.:
			return ":op15"
		if x<=EDGE_TYPE_SEM_LOCATION+1/2.:
			return ":location"
		if x<=EDGE_TYPE_SEM_PURPOSE+1/2.:
			return ":purpose"
		if x<=EDGE_TYPE_SEM_QUANT+1/2.:
			return ":quant"
		if x<=EDGE_TYPE_SEM_MANNER+1/2.:
			return ":manner"
		if x<=EDGE_TYPE_SEM_MEDIUM+1/2.:
			return ":medium"
		if x<=EDGE_TYPE_SEM_DURATION+1/2.:
			return ":duration"
		if x<=EDGE_TYPE_SEM_ORD+1/2.:
			return ":ord"
		if x<=EDGE_TYPE_SEM_DIRECTION+1/2.:
			return ":direction"
		if x<=EDGE_TYPE_SEM_CONDITION+1/2.:
			return ":condition"
		if x<=EDGE_TYPE_SEM_BENEFICIARY+1/2.:
			return ":beneficiary"
		if x<=EDGE_TYPE_SEM_WITHOUT+1/2.:
			return ":prep-without"
		if x<=EDGE_TYPE_SEM_SOURCE+1/2.:
			return ":source"
		if x<=EDGE_TYPE_SEM_DESTINATION+1/2.:
			return ":destination"
		if x<=EDGE_TYPE_SEM_UNIT+1/2.:
			return ":unit"
		if x<=EDGE_TYPE_SEM_CONCESSION+1/2.:
			return ":concession"
		if x<=EDGE_TYPE_SEM_IN+1/2.:
			return ":prep-in"
		if x<=EDGE_TYPE_SEM_AS+1/2.:
			return ":prep-as"
		if x<=EDGE_TYPE_SEM_INSTRUMENT+1/2.:
			return ":instrument"
		if x<=EDGE_TYPE_SEM_EXAMPLE+1/2.:
			return ":example"
		if x<=EDGE_TYPE_SEM_WITH+1/2.:
			return ":prep-with"
		if x<=EDGE_TYPE_SEM_FOR+1/2.:
			return ":prep-for"
		if x<=EDGE_TYPE_SEM_TO+1/2.:
			return ":prep-to"
		if x<=EDGE_TYPE_SEM_PATH+1/2.:
			return ":path"
		if x<=EDGE_TYPE_SEM_EXTENT+1/2.:
			return ":extent"
		if x<=EDGE_TYPE_SEM_ACCOMPANIER+1/2.:
			return ":accompanier"
		if x<=EDGE_TYPE_SEM_SUBEVENT+1/2.:
			return ":subevent"
		if x<=EDGE_TYPE_SEM_INSTANCE+1/2.:
			return ":instance"
		#warnings.warn("Missing semantic edge type decoding for "+str(x))
		return None

	def __determine_syntax_edge_type(self,src_node_id,dest_node_id,src_synid,dest_synid):
		# graph translated form is nodes, edges, leaves
		if src_synid not in self.syntax_tree[0] or dest_synid not in self.syntax_tree[0]:
			return EDGE_TYPE_NO_RELATION
		if src_node_id==dest_node_id:
			return (self.__encode_syntax_info(src_synid)-EDGE_TYPE_SYN_MIN)/(EDGE_TYPE_SYN_MAX-EDGE_TYPE_SYN_MIN)
		else:
			if src_synid in self.phrase_binding:
				level,src_head=self.phrase_binding[src_synid]
				if src_head==dest_synid:
					return (self.__encode_syntax_level(level,src_head,dest_synid)-EDGE_TYPE_SYN_MIN)/(EDGE_TYPE_SYN_MAX-EDGE_TYPE_SYN_MIN)
		return EDGE_TYPE_NO_RELATION

	def __encode_syntax_level(self,level,src_head,dest_synid):
		if level==1:
			return EDGE_TYPE_SYN_01
		if level==2:
			return EDGE_TYPE_SYN_02
		if level==3:
			return EDGE_TYPE_SYN_03
		if level==4:
			return EDGE_TYPE_SYN_04
		if level==5:
			return EDGE_TYPE_SYN_05
		if level==6:
			return EDGE_TYPE_SYN_06
		if level==7:
			return EDGE_TYPE_SYN_07
		if level==8:
			return EDGE_TYPE_SYN_08
		if level==9:
			return EDGE_TYPE_SYN_09
		if level==10:
			return EDGE_TYPE_SYN_10
		if level==11:
			return EDGE_TYPE_SYN_11
		if level==12:
			return EDGE_TYPE_SYN_12
		if level==13:
			return EDGE_TYPE_SYN_13
		if level==14:
			return EDGE_TYPE_SYN_14
		if level==15:
			return EDGE_TYPE_SYN_15
		if level==16:
			return EDGE_TYPE_SYN_16
		if level==17:
			return EDGE_TYPE_SYN_17
		if level==18:
			return EDGE_TYPE_SYN_18
		if level==19:
			return EDGE_TYPE_SYN_19
		if level==20:
			return EDGE_TYPE_SYN_20
		if level==21:
			return EDGE_TYPE_SYN_21
		if level==22:
			return EDGE_TYPE_SYN_22
		if level==23:
			return EDGE_TYPE_SYN_23
		if level==24:
			return EDGE_TYPE_SYN_24
		if level==25:
			return EDGE_TYPE_SYN_25
		if level==26:
			return EDGE_TYPE_SYN_26
		if level==27:
			return EDGE_TYPE_SYN_27
		if level==28:
			return EDGE_TYPE_SYN_28
		if level==29:
			return EDGE_TYPE_SYN_29
		if level==30:
			return EDGE_TYPE_SYN_30
		if level==31:
			return EDGE_TYPE_SYN_31
		if level==32:
			return EDGE_TYPE_SYN_32
		if level==33:
			return EDGE_TYPE_SYN_33
		if level==34:
			return EDGE_TYPE_SYN_34
		if level==35:
			return EDGE_TYPE_SYN_35
		if level==36:
			return EDGE_TYPE_SYN_36
		if level==37:
			return EDGE_TYPE_SYN_37
		if level==38:
			return EDGE_TYPE_SYN_38
		if level==39:
			return EDGE_TYPE_SYN_39
		if level==40:
			return EDGE_TYPE_SYN_40
		warnings.warn("Missing syntactic edge type encoding for "+str(level)+" ( "+str(src_head)+", "+str(dest_synid)+" )")
		return EDGE_TYPE_SYN_UNSPECIFIED

	def __decode_syntax_level(self,level):
		level=level*(EDGE_TYPE_SYN_MAX-EDGE_TYPE_SYN_MIN)+EDGE_TYPE_SYN_MIN
		if level<EDGE_TYPE_SYN_MIN-1/2. or level>EDGE_TYPE_SYN_MAX+1/2.:
			#warnings.warn("Missing syntactic edge type decoding for "+str(level))
			return None
		if level<=EDGE_TYPE_SYN_UNSPECIFIED+1/2.:
			return None
		if level<=EDGE_TYPE_SYN_01+1/2.:
			return "dist:1"
		if level<=EDGE_TYPE_SYN_02+1/2.:
			return "dist:2"
		if level<=EDGE_TYPE_SYN_03+1/2.:
			return "dist:3"
		if level<=EDGE_TYPE_SYN_04+1/2.:
			return "dist:4"
		if level<=EDGE_TYPE_SYN_05+1/2.:
			return "dist:5"
		if level<=EDGE_TYPE_SYN_06+1/2.:
			return "dist:6"
		if level<=EDGE_TYPE_SYN_07+1/2.:
			return "dist:7"
		if level<=EDGE_TYPE_SYN_08+1/2.:
			return "dist:8"
		if level<=EDGE_TYPE_SYN_09+1/2.:
			return "dist:9"
		if level<=EDGE_TYPE_SYN_10+1/2.:
			return "dist:10"
		if level<=EDGE_TYPE_SYN_11+1/2.:
			return "dist:11"
		if level<=EDGE_TYPE_SYN_12+1/2.:
			return "dist:12"
		if level<=EDGE_TYPE_SYN_13+1/2.:
			return "dist:13"
		if level<=EDGE_TYPE_SYN_14+1/2.:
			return "dist:14"
		if level<=EDGE_TYPE_SYN_15+1/2.:
			return "dist:15"
		if level<=EDGE_TYPE_SYN_16+1/2.:
			return "dist:16"
		if level<=EDGE_TYPE_SYN_17+1/2.:
			return "dist:17"
		if level<=EDGE_TYPE_SYN_18+1/2.:
			return "dist:18"
		if level<=EDGE_TYPE_SYN_19+1/2.:
			return "dist:19"
		if level<=EDGE_TYPE_SYN_20+1/2.:
			return "dist:20"
		if level<=EDGE_TYPE_SYN_21+1/2.:
			return "dist:21"
		if level<=EDGE_TYPE_SYN_22+1/2.:
			return "dist:22"
		if level<=EDGE_TYPE_SYN_23+1/2.:
			return "dist:23"
		if level<=EDGE_TYPE_SYN_24+1/2.:
			return "dist:24"
		if level<=EDGE_TYPE_SYN_25+1/2.:
			return "dist:25"
		if level<=EDGE_TYPE_SYN_26+1/2.:
			return "dist:26"
		if level<=EDGE_TYPE_SYN_27+1/2.:
			return "dist:27"
		if level<=EDGE_TYPE_SYN_28+1/2.:
			return "dist:28"
		if level<=EDGE_TYPE_SYN_29+1/2.:
			return "dist:29"
		if level<=EDGE_TYPE_SYN_30+1/2.:
			return "dist:30"
		if level<=EDGE_TYPE_SYN_31+1/2.:
			return "dist:31"
		if level<=EDGE_TYPE_SYN_32+1/2.:
			return "dist:32"
		if level<=EDGE_TYPE_SYN_33+1/2.:
			return "dist:33"
		if level<=EDGE_TYPE_SYN_34+1/2.:
			return "dist:34"
		if level<=EDGE_TYPE_SYN_35+1/2.:
			return "dist:35"
		if level<=EDGE_TYPE_SYN_36+1/2.:
			return "dist:36"
		if level<=EDGE_TYPE_SYN_37+1/2.:
			return "dist:37"
		if level<=EDGE_TYPE_SYN_38+1/2.:
			return "dist:38"
		if level<=EDGE_TYPE_SYN_39+1/2.:
			return "dist:39"
		if level<=EDGE_TYPE_SYN_40+1/2.:
			return "dist:40"
		#warnings.warn("Missing syntactic edge type decoding for "+str(level))
		return None

	def __encode_syntax_info(self,src_synid):
		if self.syntax_tree[0][src_synid]=="NN":
			return EDGE_TYPE_SYN_01
		if self.syntax_tree[0][src_synid]=="NNS":
			return EDGE_TYPE_SYN_02
		if self.syntax_tree[0][src_synid]=="PRP":
			return EDGE_TYPE_SYN_03
		if self.syntax_tree[0][src_synid]=="PRP$":
			return EDGE_TYPE_SYN_04
		if self.syntax_tree[0][src_synid]=="EX":
			return EDGE_TYPE_SYN_05
		if self.syntax_tree[0][src_synid]=="WP":
			return EDGE_TYPE_SYN_06
		if self.syntax_tree[0][src_synid]=="WP$":
			return EDGE_TYPE_SYN_07
		if self.syntax_tree[0][src_synid]=="NNP":
			return EDGE_TYPE_SYN_08
		if self.syntax_tree[0][src_synid]=="NNPS":
			return EDGE_TYPE_SYN_09
		if self.syntax_tree[0][src_synid]=="FW":
			return EDGE_TYPE_SYN_10
		if self.syntax_tree[0][src_synid]=="RP":
			return EDGE_TYPE_SYN_11
		if self.syntax_tree[0][src_synid]=="SYM":
			return EDGE_TYPE_SYN_12
		if self.syntax_tree[0][src_synid]=="UH":
			return EDGE_TYPE_SYN_13
		if self.syntax_tree[0][src_synid]=="LS":
			return EDGE_TYPE_SYN_14
		if self.syntax_tree[0][src_synid]=="DT":
			return EDGE_TYPE_SYN_15
		if self.syntax_tree[0][src_synid]=="WDT":
			return EDGE_TYPE_SYN_16
		if self.syntax_tree[0][src_synid]=="PDT":
			return EDGE_TYPE_SYN_17
		if self.syntax_tree[0][src_synid]=="VB":
			return EDGE_TYPE_SYN_18
		if self.syntax_tree[0][src_synid]=="VBD":
			return EDGE_TYPE_SYN_19
		if self.syntax_tree[0][src_synid]=="VBZ":
			return EDGE_TYPE_SYN_20
		if self.syntax_tree[0][src_synid]=="VBG":
			return EDGE_TYPE_SYN_21
		if self.syntax_tree[0][src_synid]=="VBP":
			return EDGE_TYPE_SYN_22
		if self.syntax_tree[0][src_synid]=="MD":
			return EDGE_TYPE_SYN_23
		if self.syntax_tree[0][src_synid]=="VBN":
			return EDGE_TYPE_SYN_24
		if self.syntax_tree[0][src_synid]=="IN":
			return EDGE_TYPE_SYN_25
		if self.syntax_tree[0][src_synid]=="TO":
			return EDGE_TYPE_SYN_26
		if self.syntax_tree[0][src_synid]=="JJ":
			return EDGE_TYPE_SYN_27
		if self.syntax_tree[0][src_synid]=="JJR":
			return EDGE_TYPE_SYN_28
		if self.syntax_tree[0][src_synid]=="JJS":
			return EDGE_TYPE_SYN_29
		if self.syntax_tree[0][src_synid]=="CD":
			return EDGE_TYPE_SYN_30
		if self.syntax_tree[0][src_synid]=="CC":
			return EDGE_TYPE_SYN_31
		if self.syntax_tree[0][src_synid]=="RB":
			return EDGE_TYPE_SYN_32
		if self.syntax_tree[0][src_synid]=="WRB":
			return EDGE_TYPE_SYN_33
		if self.syntax_tree[0][src_synid]=="RBR":
			return EDGE_TYPE_SYN_34
		if self.syntax_tree[0][src_synid]=="RBS":
			return EDGE_TYPE_SYN_35
		if self.syntax_tree[0][src_synid]==".":
			return EDGE_TYPE_SYN_36
		if self.syntax_tree[0][src_synid]=="POS":
			return EDGE_TYPE_SYN_37
		warnings.warn("Missing syntactic info encoding for "+str(self.syntax_tree[0][src_synid]))
		return EDGE_TYPE_SYN_UNSPECIFIED

	def __decode_syntax_info(self,val):
		val=val*(EDGE_TYPE_SYN_MAX-EDGE_TYPE_SYN_MIN)+EDGE_TYPE_SYN_MIN
		if val<EDGE_TYPE_SYN_MIN-1/2. or val>EDGE_TYPE_SYN_MAX+1/2.:
			#warnings.warn("Missing syntactic info decoding for "+str(val))
			return None
		if val<=EDGE_TYPE_SYN_UNSPECIFIED+1/2.:
			return None
		if val<=EDGE_TYPE_SYN_01+1/2.:
			return "NN"
		if val<=EDGE_TYPE_SYN_02+1/2.:
			return "NNS"
		if val<=EDGE_TYPE_SYN_03+1/2.:
			return "PRP"
		if val<=EDGE_TYPE_SYN_04+1/2.:
			return "PRP$"
		if val<=EDGE_TYPE_SYN_05+1/2.:
			return "EX"
		if val<=EDGE_TYPE_SYN_06+1/2.:
			return "WP"
		if val<=EDGE_TYPE_SYN_07+1/2.:
			return "WP$"
		if val<=EDGE_TYPE_SYN_08+1/2.:
			return "NNP"
		if val<=EDGE_TYPE_SYN_09+1/2.:
			return "NNPS"
		if val<=EDGE_TYPE_SYN_10+1/2.:
			return "FW"
		if val<=EDGE_TYPE_SYN_11+1/2.:
			return "RP"
		if val<=EDGE_TYPE_SYN_12+1/2.:
			return "SYM"
		if val<=EDGE_TYPE_SYN_13+1/2.:
			return "UH"
		if val<=EDGE_TYPE_SYN_14+1/2.:
			return "LS"
		if val<=EDGE_TYPE_SYN_15+1/2.:
			return "DT"
		if val<=EDGE_TYPE_SYN_16+1/2.:
			return "WDT"
		if val<=EDGE_TYPE_SYN_17+1/2.:
			return "PDT"
		if val<=EDGE_TYPE_SYN_18+1/2.:
			return "VB"
		if val<=EDGE_TYPE_SYN_19+1/2.:
			return "VBD"
		if val<=EDGE_TYPE_SYN_20+1/2.:
			return "VBZ"
		if val<=EDGE_TYPE_SYN_21+1/2.:
			return "VBG"
		if val<=EDGE_TYPE_SYN_22+1/2.:
			return "VBP"
		if val<=EDGE_TYPE_SYN_23+1/2.:
			return "MD"
		if val<=EDGE_TYPE_SYN_24+1/2.:
			return "VBN"
		if val<=EDGE_TYPE_SYN_25+1/2.:
			return "IN"
		if val<=EDGE_TYPE_SYN_26+1/2.:
			return "TO"
		if val<=EDGE_TYPE_SYN_27+1/2.:
			return "JJ"
		if val<=EDGE_TYPE_SYN_28+1/2.:
			return "JJR"
		if val<=EDGE_TYPE_SYN_29+1/2.:
			return "JJS"
		if val<=EDGE_TYPE_SYN_30+1/2.:
			return "CD"
		if val<=EDGE_TYPE_SYN_31+1/2.:
			return "CC"
		if val<=EDGE_TYPE_SYN_32+1/2.:
			return "RB"
		if val<=EDGE_TYPE_SYN_33+1/2.:
			return "WRB"
		if val<=EDGE_TYPE_SYN_34+1/2.:
			return "RBR"
		if val<=EDGE_TYPE_SYN_35+1/2.:
			return "RBS"
		if val<=EDGE_TYPE_SYN_36+1/2.:
			return "."
		if val<=EDGE_TYPE_SYN_37+1/2.:
			return "POS"
		#warnings.warn("Missing syntactic info decoding for "+str(val))
		return None

	def __determine_edge_type(self,layer,src_node_id,dest_node_id):
		if src_node_id not in self.lexical_nodes:
			return EDGE_TYPE_NO_RELATION
		if dest_node_id not in self.lexical_nodes:
			return EDGE_TYPE_NO_RELATION
		src_semvar=self.semantic_nodes[src_node_id]
		dest_semvar=self.semantic_nodes[dest_node_id]
		src_synvar=self.syntax_nodes[src_node_id]
		dest_synvar=self.syntax_nodes[dest_node_id]
		if layer==0:
			return self.__determine_syntax_edge_type(src_node_id,dest_node_id,src_synvar,dest_synvar)
		elif layer==1:
			return self.__determine_semantic_edge_type(src_node_id,dest_node_id,src_semvar,dest_semvar)
		return EDGE_TYPE_NO_RELATION

	def to_str(self):
		return " ".join(self.lexical_nodes[i].decode('ascii','replace') for i in self.lexical_nodes)

	def to_dot_syntactic(self):
		nodes={} # todo this should be in the field
		edges=[]
		for ele in self.syntax_graph:
			start=ele[0]
			end=ele[2]
			type=ele[1]
			snode=self.lexical_nodes[start]
			enode=self.lexical_nodes[end]
			nodes[start]=snode
			nodes[end]=enode
			edges.append((start,type,end))
		s="digraph{\n"
		for var,label in nodes.items():
			s+="\ta"+str(var)+" [label=\""+str(label)+"\"]\n"
		for edge in edges:
			s+="\ta"+str(edge[0])+" -> a"+str(edge[2])+" [label=\""+str(edge[1])+"\"]\n"

		s+="\tlabelloc=\"t\";\nlabel=\"Syntactic graph for '"+self.to_str()+"'\";\n}"

		return s


	def to_dot_semantic(self):
		nodes={} # todo this should be in the field
		edges=[]
		for ele in self.semantic_graph:
			start=ele[0]
			end=ele[2]
			type=ele[1]
			snode=self.lexical_nodes[start]
			enode=self.lexical_nodes[end]
			nodes[start]=snode
			nodes[end]=enode
			edges.append((start,type,end))
		s="digraph{\n"
		for var,label in nodes.items():
			s+="\ta"+str(var)+" [label=\""+str(label)+"\"]\n"
		for edge in edges:
			s+="\ta"+str(edge[0])+" -> a"+str(edge[2])+" [label=\""+str(edge[1])+"\"]\n"

		s+="\tlabelloc=\"t\";\nlabel=\"Semantic graph for '"+self.to_str()+"'\";\n}"

		return s
