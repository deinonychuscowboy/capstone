import warnings

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, backend
import numpy

class NN(keras.Model):
	def get_config(self):
		pass

	def call(self,inputs,training=None,mask=None):
		pass

	def __init__(self, transformer, args):
		super(NN, self).__init__()
		self.transformer = transformer
		self.args_obj=args
		self.trans_loss_tracker = keras.metrics.Mean(name="transformer_loss")

	@property
	def metrics(self):
		return [self.trans_loss_tracker]

	def compile(self,t_optimzer,t_loss_fn,**kwargs):
		super(NN, self).compile()
		self.t_optimizer = t_optimzer
		self.t_loss_fn = t_loss_fn

	def compute_t_loss(self,labels,predictions):
		per_example_loss=self.t_loss_fn(labels,predictions)
		return tf.nn.compute_average_loss(per_example_loss,global_batch_size=self.args_obj.global_batch_size)

	def train_step(self, data):
		real_data, real_labels = data

		with tf.GradientTape() as tape:
			predictions = self.transformer(real_data)
			trans_loss = self.compute_t_loss(real_labels, predictions)
			grad = tape.gradient(trans_loss, self.transformer.trainable_weights)
			self.t_optimizer.apply_gradients(zip(grad, self.discriminator.trainable_weights))

		self.trans_loss_tracker.update_state(trans_loss)
		return {
			"t_loss": self.trans_loss_tracker.result()
		}

class NNBuilder:
	def __init__(self,latent_dim,global_batch_size,epoch_count):
		self.latent_dim=latent_dim
		self.global_batch_size=global_batch_size
		self.epoch_count=epoch_count
		self.sample_size=0
		self.component_size=0
		self.codec_depth=0


	def build(self,sample_size,component_size,codec_depth):
		self.sample_size=sample_size
		self.component_size=component_size
		self.codec_depth=codec_depth

		strategy=tf.distribute.MirroredStrategy()

		with strategy.scope():
			transformer=keras.Sequential(
				[
					keras.layers.InputLayer(
						(sample_size,component_size,codec_depth)),
					layers.Dense(sample_size*component_size*codec_depth),
					layers.LeakyReLU(alpha=0.2),
					layers.Dense(sample_size*component_size*codec_depth/2),
					layers.LeakyReLU(alpha=0.2),
					layers.Dense(self.latent_dim),
					layers.LeakyReLU(alpha=0.2),
					layers.Dense(sample_size*component_size*codec_depth/2),
					layers.LeakyReLU(alpha=0.2),
					layers.Dense(sample_size*component_size*codec_depth),
				],
				name="transformer",
			)

			nn=NN(
				transformer,self
			)
			nn.compile(keras.optimizers.Adam(learning_rate=0.0003),keras.losses.BinaryCrossentropy(from_logits=True,reduction=tf.keras.losses.Reduction.NONE))

		return nn
