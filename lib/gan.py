import warnings

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, backend
import numpy, os

class ConditionalGAN(keras.Model):
	def get_config(self):
		pass

	def call(self,inputs,training=None,mask=None):
		pass

	def __init__(self, discriminator, generator, args):
		super(ConditionalGAN, self).__init__()
		self.discriminator = discriminator
		self.generator = generator
		self.args_obj=args
		self.gen_loss_tracker = keras.metrics.Mean(name="generator_loss")
		self.disc_loss_tracker = keras.metrics.Mean(name="discriminator_loss")

	@property
	def metrics(self):
		return [self.gen_loss_tracker, self.disc_loss_tracker]

	def compile(self,g_optimizer,d_optimizer,g_loss_fn,d_loss_fn,**kwargs):
		super(ConditionalGAN, self).compile()
		self.d_optimizer = d_optimizer
		self.g_optimizer = g_optimizer
		self.d_loss_fn = d_loss_fn
		self.g_loss_fn = g_loss_fn

	def compute_d_loss(self,labels,predictions):
		per_example_loss=self.d_loss_fn(labels,predictions)
		return tf.nn.compute_average_loss(per_example_loss,global_batch_size=self.args_obj.global_batch_size)

	def compute_g_loss(self,labels,predictions):
		per_example_loss=self.g_loss_fn(labels,predictions)
		return tf.nn.compute_average_loss(per_example_loss,global_batch_size=self.args_obj.global_batch_size)

	@staticmethod
	def convert_labels(args_obj,labels):
		one_hot_vectors = labels[:, :, None, None]
		one_hot_vectors = tf.repeat(
			one_hot_vectors, repeats=[args_obj.sample_size * args_obj.sample_size]
		)
		return tf.reshape(
			one_hot_vectors, (-1, args_obj.sample_size, args_obj.sample_size, args_obj.num_classes)
		)

	def train_step(self, data):
		# build training set for discriminator from real data and generator data
		real_data, real_labels = data
		real_labels_vector = self.convert_labels(self.args_obj,real_labels)
		real_categorized = tf.concat([real_data, real_labels_vector], -1)

		batch_size = tf.shape(real_data)[0]
		random_latent = tf.random.normal(shape=(batch_size, self.args_obj.latent_dim))
		random_categorized = tf.concat([random_latent, real_labels], axis=1)
		generated = self.generator(random_categorized)
		generated_categorized = tf.concat([generated, real_labels_vector], -1)

		all_images = tf.concat([generated_categorized, real_categorized], axis=0)
		labels = tf.concat([tf.ones((batch_size, 1)), tf.zeros((batch_size, 1))], axis=0)

		# train discriminator with combined dataset
		with tf.GradientTape() as tape:
			predictions = self.discriminator(all_images)
			discrim_loss = self.compute_d_loss(labels, predictions)
			grad = tape.gradient(discrim_loss, self.discriminator.trainable_weights)
			self.d_optimizer.apply_gradients(zip(grad, self.discriminator.trainable_weights))

		# build training set for generator with random latent space
		random_latent = tf.random.normal(shape=(batch_size, self.args_obj.latent_dim))
		random_categorized = tf.concat([random_latent, real_labels], axis=1)
		unknown_labels = tf.zeros((batch_size, 1))

		# train generator
		with tf.GradientTape() as tape:
			generated = self.generator(random_categorized)
			generated_categorized = tf.concat([generated, real_labels_vector], -1)
			predictions = self.discriminator(generated_categorized)
			gen_loss = self.compute_g_loss(unknown_labels, predictions) # loss based on discriminator's predictions
			grad = tape.gradient(gen_loss, self.generator.trainable_weights)
			self.g_optimizer.apply_gradients(zip(grad, self.generator.trainable_weights))

		# final output
		self.gen_loss_tracker.update_state(gen_loss)
		self.disc_loss_tracker.update_state(discrim_loss)
		return {
			"g_loss": self.gen_loss_tracker.result(),
			"d_loss": self.disc_loss_tracker.result(),
		}

class GANBuilder:
	def __init__(self,latent_dim,global_batch_size,epoch_count,metric_size,used_graph_layers):
		self.latent_dim=latent_dim
		self.num_channels=0
		self.num_classes=0
		self.global_batch_size=global_batch_size
		self.epoch_count=epoch_count
		self.metric_size=metric_size
		self.sample_size=0
		self.used_graph_layers=used_graph_layers

	def __excess_channel_depth_perfusion(self,model_perfusion,vector_diameter,vector_depth):
		p=[]
		for x in range(0,vector_diameter):
			q=[]
			for y in range(0,vector_diameter):
				r=[]
				for z in range(0,model_perfusion):
					r.append(0)
				for z in range(model_perfusion,vector_depth):
					r.append(1 if y>0 and x>0 and y!=x else 0)
				q.append(r)
			p.append(q)
		p=numpy.array(p,dtype='float32')
		def perfPenalty(pred):
			if len(pred.shape)!=4:
				#warnings.warn("Loss error (This is normal before the first epoch begins)")
				return pred*0
			return tf.tensordot(pred,p,((1,2,3),(0,1,2)))
		return perfPenalty

	def __nonsymmetric_graph_template(self):
		def symPenalty(pred):
			if len(pred.shape)!=4:
				#warnings.warn("Loss error (This is normal before the first epoch begins)")
				return pred*0
			first_sequence=pred[:,:1,1:,:]
			second_sequence=pred[:,1:,:1,:]
			nonsymmetric_loss=[]
			if first_sequence.shape[0] is not None:
				for i in range(0,first_sequence.shape[0]):
					loss_accumulator=0
					for j in range(0,first_sequence.shape[1]):
						for k in range(0,first_sequence.shape[2]):
							for l in range(0,first_sequence.shape[3]):
								loss_accumulator+=abs(first_sequence[i,j,k,l]-second_sequence[i,k,j,l])
					nonsymmetric_loss.append(loss_accumulator)
			return numpy.array(nonsymmetric_loss,dtype='float32')
		return symPenalty

	def __identical_graph_nodes(self):
		def idPenalty(pred):
			if len(pred.shape)!=4:
				#warnings.warn("Loss error (This is normal before the first epoch begins)")
				return pred*0
			first_sequence=pred[:,:1,1:,:]
			identical_loss=[]
			if first_sequence.shape[0] is not None:
				for i in range(0,first_sequence.shape[0]):
					loss_accumulator=0
					map={}
					for j in range(0,first_sequence.shape[1]):
						if first_sequence[i,j,0] not in map:
							map[first_sequence[i,j,0]]=0
						if numpy.sum(first_sequence[i,j,0])!=0:
							loss_accumulator+=map[first_sequence[i,j,0]]
						map[first_sequence[i,j,0]]+=1
					identical_loss.append(loss_accumulator)
			return numpy.array(identical_loss,dtype='float32')
		return idPenalty

	def __size_scale(self):
		def sizePenalty(pred):
			if len(pred.shape)!=4:
				#warnings.warn("Loss error (This is normal before the first epoch begins)")
				return pred*0
			sizes=pred[:,0,0,0]
			first_sequence=pred[:,:1,1:,:]
			size_loss=[]
			if first_sequence.shape[0] is not None:
				for i in range(0,first_sequence.shape[0]):
					size=sizes[i]
					estimated_size=0
					for j in range(0,first_sequence.shape[1]):
						if int(round(first_sequence[i,j,0]))!=0:
							estimated_size=j
					size_loss.append(min(0,abs(size-estimated_size),2-size))
			return numpy.array(size_loss,dtype='float32')
		return sizePenalty

	def __penalizedLoss(self,from_logits):
		primary_loss_fn=keras.losses.BinaryCrossentropy(from_logits=from_logits,reduction=tf.keras.losses.Reduction.NONE)
		perfusion_loss=self.__excess_channel_depth_perfusion(self.used_graph_layers,self.sample_size,self.num_channels)
		symmetric_loss=self.__nonsymmetric_graph_template()
		identical_loss=self.__identical_graph_nodes()
		size_loss=self.__size_scale()
		perfusion_loss_weight=float(os.environ["ECDP_WEIGHT"]) if "ECDP_WEIGHT" in os.environ else 0.05
		symmetric_loss_weight=float(os.environ["NSGT_WEIGHT"]) if "NSGT_WEIGHT" in os.environ else 0.05
		identical_loss_weight=float(os.environ["IGN_WEIGHT"]) if "IGN_WEIGHT" in os.environ else 0.05
		size_loss_weight=float(os.environ["SC_WEIGHT"]) if "SC_WEIGHT" in os.environ else 0.05
		def ls(true,pred):
			return primary_loss_fn(true,pred)+perfusion_loss(pred)*perfusion_loss_weight+symmetric_loss(pred)*symmetric_loss_weight+identical_loss(pred)*identical_loss_weight+size_loss(pred)*size_loss_weight
		return ls

	def __basicLoss(self,from_logits):
		primary_loss_fn=keras.losses.BinaryCrossentropy(from_logits=from_logits,reduction=tf.keras.losses.Reduction.NONE)
		def ls(true,pred):
			return primary_loss_fn(true,pred)
		return ls

	def build(self,sample_size,categorical_size,channel_size):
		self.sample_size=sample_size
		self.num_classes=categorical_size
		self.num_channels=channel_size
		generator_in_channels=self.latent_dim+self.num_classes
		discriminator_in_channels=self.num_channels+self.num_classes

		strategy=tf.distribute.MirroredStrategy()
		discriminator=None
		generator=None
		activate_discrim="ACTIVATE" not in os.environ or os.environ["ACTIVATE"]=="1" or os.environ["ACTIVATE"]=="2"
		activate_gen="ACTIVATE" in os.environ and os.environ["ACTIVATE"]=="1" or os.environ["ACTIVATE"]=="3"

		with strategy.scope():
			if "STRATEGY" in os.environ and os.environ["STRATEGY"]=="dense":
				discriminator=keras.Sequential(
					[
						layers.InputLayer((self.sample_size,self.sample_size,discriminator_in_channels)),
						layers.Reshape((self.sample_size*self.sample_size*discriminator_in_channels,)),
						layers.LeakyReLU(alpha=0.2),
						layers.Dense(self.latent_dim*2),
						layers.LeakyReLU(alpha=0.2),
						layers.Dense(self.latent_dim),
						layers.Reshape((self.latent_dim,1)),
						layers.GlobalMaxPooling1D(),
						layers.Dense(1,activation="sigmoid" if activate_discrim else None)
					],
					name="discriminator",
				)
				generator=keras.Sequential(
					[
						layers.InputLayer((generator_in_channels,)),
						layers.Dense(self.latent_dim),
						layers.LeakyReLU(alpha=0.2),
						layers.Dense(self.latent_dim*2),
						layers.LeakyReLU(alpha=0.2),
						layers.Dense(self.num_channels*self.sample_size*self.sample_size,activation="sigmoid" if activate_gen else None),
						layers.Reshape((self.sample_size,self.sample_size,self.num_channels))
					],
					name="generator",
				)
			elif "STRATEGY" in os.environ and os.environ["STRATEGY"]=="conv":
				discriminator=keras.Sequential(
					[
						layers.InputLayer((self.sample_size,self.sample_size,discriminator_in_channels)),
						layers.Conv2D(64,(3,3),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same"),
						layers.LeakyReLU(alpha=0.2),
						layers.Conv2D(128,(3,3),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same"),
						layers.LeakyReLU(alpha=0.2),
						layers.GlobalMaxPooling2D(),
						layers.Dense(1,activation="sigmoid" if activate_discrim else None)
					],
					name="discriminator",
				)
				generator=keras.Sequential(
					[
						layers.InputLayer((generator_in_channels,)),
						layers.Dense(int(self.sample_size/self.metric_size*sample_size/self.metric_size*generator_in_channels)), # start with sample_size/metric_size values
						layers.LeakyReLU(alpha=0.2),
						layers.Reshape((int(self.sample_size/self.metric_size),int(self.sample_size/self.metric_size),generator_in_channels)),
						layers.Conv2DTranspose(128,(self.metric_size,self.metric_size),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same"), # grow by (metric_size/2)
						layers.LeakyReLU(alpha=0.2),
						layers.Conv2DTranspose(128,(self.metric_size,self.metric_size),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same"), # grow by (metric_size/2)
						layers.LeakyReLU(alpha=0.2),
						layers.Conv2D(self.num_channels,(self.sample_size,self.sample_size),padding="same",activation="sigmoid" if activate_gen else None),
						layers.Reshape((self.sample_size,self.sample_size,self.num_channels))
					],
					name="generator",
				)
			elif "STRATEGY" in os.environ and os.environ["STRATEGY"]=="lstm":
				codec_input=layers.Input((self.sample_size,self.sample_size,discriminator_in_channels))
				encoder_reshape=layers.Reshape((self.sample_size,self.sample_size*discriminator_in_channels))(codec_input)
				encoder_output, encoder_state_h, encoder_state_c=layers.LSTM(64,input_shape=(self.sample_size,self.sample_size*discriminator_in_channels),return_state=True)(encoder_reshape)
				encoder_state=(encoder_state_h, encoder_state_c)
				decoder_transpose=layers.Permute(dims=(2,1,3))(codec_input)
				decoder_reshape=layers.Reshape((self.sample_size,self.sample_size*discriminator_in_channels))(decoder_transpose)
				decoder_output=layers.LSTM(64,input_shape=(self.sample_size,self.sample_size*discriminator_in_channels))(decoder_reshape,initial_state=encoder_state)
				output=layers.Dense(1,activation="sigmoid" if activate_discrim else None)(decoder_output)
				discriminator=keras.Model(
					codec_input,
					output,
					name="discriminator",
				)
				codec_input=layers.Input((generator_in_channels,))
				encoder_reshape=layers.Reshape((generator_in_channels,1))(codec_input)
				encoder_output, encoder_state_h, encoder_state_c=layers.LSTM(64,input_shape=(generator_in_channels,1),return_state=True)(encoder_reshape)
				encoder_state=(encoder_state_h, encoder_state_c)
				decoder_reshape=layers.Reshape((generator_in_channels,1))(codec_input)
				decoder_output=layers.LSTM(64,input_shape=(generator_in_channels,1))(decoder_reshape,initial_state=encoder_state)
				output=layers.Dense(self.sample_size*self.sample_size*self.num_channels,activation="sigmoid" if activate_gen else None)(decoder_output)
				output_reshape=layers.Reshape((self.sample_size,self.sample_size,self.num_channels))(output)
				generator=keras.Model(
					codec_input,
					output_reshape,
					name="generator",
				)
			elif "STRATEGY" in os.environ and os.environ["STRATEGY"]=="attn":
				qv_input=layers.Input((self.sample_size,self.sample_size,discriminator_in_channels))
				v_transpose=layers.Permute(dims=(2,1,3))(qv_input)
				cnn=layers.Conv2D(64,(3,3),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same")
				query_encoding=cnn(qv_input)
				value_encoding=cnn(v_transpose)
				attention=layers.Attention()([query_encoding,value_encoding])
				query_output=layers.GlobalAveragePooling2D()(query_encoding)
				value_output=layers.GlobalAveragePooling2D()(attention)
				combined=layers.Concatenate()([query_output,value_output])
				output=layers.Dense(1,activation="sigmoid" if activate_discrim else None)(combined)
				discriminator=keras.Model(
					qv_input,
					output,
					name="discriminator",
				)
				qv_input=layers.Input((generator_in_channels,))
				qv_reshape=layers.Reshape((1,1,generator_in_channels))(qv_input)
				cnn=layers.Conv2D(64,(3,3),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same")
				query_encoding=cnn(qv_reshape)
				value_encoding=cnn(qv_reshape)
				attention=layers.Attention()([query_encoding,value_encoding])
				query_output=layers.GlobalAveragePooling2D()(query_encoding)
				value_output=layers.GlobalAveragePooling2D()(attention)
				combined=layers.Concatenate()([query_output,value_output])
				output=layers.Dense(self.sample_size*self.sample_size*self.num_channels,activation="sigmoid" if activate_gen else None)(combined)
				output_reshape=layers.Reshape((self.sample_size,self.sample_size,self.num_channels))(output)
				generator=keras.Model(
					qv_input,
					output_reshape,
					name="generator",
				)
			elif "STRATEGY" in os.environ and os.environ["STRATEGY"]=="lstmattn":
				codec_input=layers.Input((self.sample_size,self.sample_size,discriminator_in_channels))
				encoder_reshape=layers.Reshape((self.sample_size,self.sample_size*discriminator_in_channels))(codec_input)
				encoder_output, encoder_state_h, encoder_state_c=layers.LSTM(64,input_shape=(self.sample_size,self.sample_size*discriminator_in_channels),return_state=True)(encoder_reshape)
				encoder_state=(encoder_state_h, encoder_state_c)
				decoder_transpose=layers.Permute(dims=(2,1,3))(codec_input)
				decoder_reshape=layers.Reshape((self.sample_size,self.sample_size*discriminator_in_channels))(decoder_transpose)
				decoder_output=layers.LSTM(64,input_shape=(self.sample_size,self.sample_size*discriminator_in_channels))(decoder_reshape,initial_state=encoder_state)
				output=layers.Dense(1,activation="sigmoid" if activate_discrim else None)(decoder_output)
				discriminator=keras.Model(
					codec_input,
					output,
					name="discriminator",
				)
				qv_input=layers.Input((generator_in_channels,))
				qv_reshape=layers.Reshape((1,1,generator_in_channels))(qv_input)
				cnn=layers.Conv2D(64,(3,3),strides=(int(self.metric_size/2),int(self.metric_size/2)),padding="same")
				query_encoding=cnn(qv_reshape)
				value_encoding=cnn(qv_reshape)
				attention=layers.Attention()([query_encoding,value_encoding])
				query_output=layers.GlobalAveragePooling2D()(query_encoding)
				value_output=layers.GlobalAveragePooling2D()(attention)
				combined=layers.Concatenate()([query_output,value_output])
				output=layers.Dense(self.sample_size*self.sample_size*self.num_channels,activation="sigmoid" if activate_gen else None)(combined)
				output_reshape=layers.Reshape((self.sample_size,self.sample_size,self.num_channels))(output)
				generator=keras.Model(
					qv_input,
					output_reshape,
					name="generator",
				)


			cond_gan=ConditionalGAN(
				discriminator,generator,self
			)
			cond_gan.compile(
				keras.optimizers.Adam(learning_rate=float(os.environ["GEN_LEARNING"]) if  "GEN_LEARNING" in os.environ else 0.0003),
				keras.optimizers.Adam(learning_rate=float(os.environ["DISC_LEARNING"]) if  "DISC_LEARNING" in os.environ else 0.0003),
				self.__penalizedLoss(True),
				self.__basicLoss(True)
			)

		return cond_gan
