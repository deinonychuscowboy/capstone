import sys

class Tee:
    def __init__(self, fd1, fd2) :
        self.__fd1 = fd1
        self.__fd2 = fd2

    def __del__(self) :
        if self.__fd1 != sys.stdout and self.__fd1 != sys.stderr :
            self.__fd1.close()
        if self.__fd2 != sys.stdout and self.__fd2 != sys.stderr :
            self.__fd2.close()

    def write(self, text) :
        self.__fd1.write(text)
        self.__fd2.write(text)

    def flush(self) :
        self.__fd1.flush()
        self.__fd2.flush()
