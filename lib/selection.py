import tensorflow as tf
from tensorflow import keras
import numpy

class Interpolator:
	def __init__(self,cond_gan,builder,count):
		self.trained_gen=cond_gan.generator
		self.builder=builder
		self.count=count

	def interpolate_class(self,n1,n2):
		assert n1>=0 and n1<self.builder.num_classes,"Starting class invalid"
		assert n2>=0 and n2<=self.builder.num_classes,"Ending class invalid"
		label1=keras.utils.to_categorical([n1],self.builder.num_classes)
		label2=keras.utils.to_categorical([n2],self.builder.num_classes)
		label1=tf.cast(label1,tf.float32)
		label2=tf.cast(label2,tf.float32)

		latent=tf.random.normal(shape=(1,self.builder.latent_dim))
		latent=tf.repeat(latent,repeats=self.count)
		latent=tf.reshape(latent,(self.count,self.builder.latent_dim))

		percent=tf.linspace(0,1,self.count)[:,None]
		percent=tf.cast(percent,tf.float32)
		labels=(label1*(1-percent)+label2*percent)

		categories=tf.concat([latent,labels],1)
		generated=self.trained_gen.predict(categories)
		return generated,labels

	def random(self,cat=0):
		labels=keras.utils.to_categorical([cat],self.builder.num_classes)
		s=labels.shape[0]
		labels=tf.cast(labels,tf.float32)
		labels=tf.repeat(labels,repeats=self.count)
		labels=tf.reshape(labels,(self.count,s))

		generated=[]
		for x in range(0,self.count):
			latent=tf.random.uniform(shape=(1,self.builder.latent_dim))
			categories=tf.concat([latent,tf.reshape(labels[x],(1,1))],1)
			generated.append(self.trained_gen.predict(categories)[0])
		return numpy.array(generated),labels
