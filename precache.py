#!/usr/bin/env python3
import nltk
nltk.download("punkt")

from datasets import get_models

get_models("coca")
get_models("dummy")
#get_models("dummy_gloss",spec_codec="fasttext:wiki.en",spec_syn=None,spec_sem=None)
#get_models("nps",spec_codec="fasttext:wiki.en",spec_syn="ud:UD_English-EWT/en_ewt-ud-train.conllu")
