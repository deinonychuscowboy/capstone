#!/usr/bin/env python3
import psutil
import humanize
import os
import GPUtil as GPU
GPUs = GPU.getGPUs()
gpu = GPUs[0]
def printm():
 process = psutil.Process(os.getpid())
 print("CPU RAM Free: " + humanize.naturalsize( psutil.virtual_memory().available ), " | Proc size: " + humanize.naturalsize( process.memory_info().rss))
 print("GPU RAM Free: {0} | Used: {1} | Util {2:3.0f}% | Total {3}".format(humanize.naturalsize(gpu.memoryFree*1024*1024), humanize.naturalsize(gpu.memoryUsed*1024*1024), gpu.memoryUtil*100, humanize.naturalsize(gpu.memoryTotal*1024*1024)))
printm()
