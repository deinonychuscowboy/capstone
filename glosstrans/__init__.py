print("Loading libraries...")
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
#os.environ['CUDA_VISIBLE_DEVICES']='-1'
import tensorflow as tf

import datasets.gloss as gloss
import datasets.gloss.dummy as ds
from lib.nn import NNBuilder

tf.get_logger().setLevel('WARN')
tf.autograph.set_verbosity(1)

builder=NNBuilder(latent_dim=128,global_batch_size=64,epoch_count=10)

dataset1,labels1,dataset2,labels2,dataset3,labels3,data_shape,codec = ds.corpus()

nn1=builder.build(sample_size=data_shape[0],component_size=data_shape[1],codec_depth=data_shape[2])
nn2=builder.build(sample_size=data_shape[0],component_size=data_shape[1],codec_depth=data_shape[2])
nn3=builder.build(sample_size=data_shape[0],component_size=data_shape[1],codec_depth=data_shape[2])
print("Beginning training...")

nn1.fit(dataset1, labels1, epochs=builder.epoch_count, batch_size=builder.global_batch_size)
nn2.fit(dataset2, labels2, epochs=builder.epoch_count, batch_size=builder.global_batch_size)
nn3.fit(dataset3, labels3, epochs=builder.epoch_count, batch_size=builder.global_batch_size)

# disable auto-sharding since we will be submitting data one piece at a time
tf.data.experimental.DistributeOptions.auto_shard_policy=tf.data.experimental.AutoShardPolicy.OFF

sample=[
	"the quick brown fox jumps over the lazy dog",
	"the quick brown fox.M.SG.NOM jumps over the lazy dog.M.SG.ACC",
	"the quick brown fox.M.SG.NOM jumps over the lazy dog.M.SG.ACC",
	"der schneller brauner fuchs springt über den trägen hund"
]
test1,tlabel1,test2,tlabel2,test3,tlabel3,data_shape,codec=gloss.process_corpus(sample,save=False,components_size=data_shape[1],sentence_size=data_shape[0])

print("This is test data:",gloss.decode(test1,codec))
res1=nn1.predict(test1,verbose=0)
res2=nn2.predict(res1,verbose=0)
res3=nn3.predict(res2,verbose=0)
print("Full pass: ",gloss.decode(res3,codec))
print()
print("Stage 1: ",gloss.decode(nn1.predict(test1,verbose=0),codec),gloss.decode(tlabel1,codec))
print("Stage 2: ",gloss.decode(nn2.predict(test2,verbose=0),codec),gloss.decode(tlabel2,codec))
print("Stage 3: ",gloss.decode(nn3.predict(test3,verbose=0),codec),gloss.decode(tlabel3,codec))
